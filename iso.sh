#!/bin/sh
set -e
. ./build.sh

rm -rfv isodir
mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub

cp sysroot/boot/lagos.kernel isodir/boot/lagos.kernel
cat > isodir/boot/grub/grub.cfg << EOF
menuentry "LagOS 0.3" {
	multiboot /boot/lagos.kernel
}
EOF
grub-mkrescue --output=lagos.iso isodir
