#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <kernel/bda.h>
#include <kernel/inlasm.h>
#include <kernel/vga.h>

size_t terminal_row;
size_t terminal_column;
u8 terminal_color;
u16* terminal_buffer;

const struct bios_data_area* bda = (struct bios_data_area*)0x0400;

void terminal_updatepos() {
	unsigned short position = terminal_row * VGA_WIDTH + terminal_column;
	
	outb(bda->vidport, 0x0F);
	outb(bda->vidport + 1, (unsigned char)(position & 0xFF));

	outb(bda->vidport, 0x0E);
	outb(bda->vidport + 1, (unsigned char)((position >> 8) & 0xFF));
}

void terminal_initialize() {
	terminal_row = 0;
	terminal_column = 0;
	terminal_color = make_color(COLOR_LIGHT_GREY, COLOR_BLACK);
	terminal_buffer = VGA_MEMORY;
	const size_t buffer_size = VGA_HEIGHT * VGA_WIDTH * 2;
	memset(terminal_buffer, 0, buffer_size);
	terminal_updatepos();
}

void terminal_setcolor(u8 color) {
	terminal_color = color;
}

void terminal_putentryat(char c, u8 color, size_t x, size_t y) {
	const size_t index = y * VGA_WIDTH + x;
	terminal_buffer[index] = make_vgaentry(c, color);
}

void terminal_pushrow() {
	u16* last_row = terminal_buffer + (VGA_HEIGHT - 1) * VGA_WIDTH;
	memmove(terminal_buffer, terminal_buffer + VGA_WIDTH, (VGA_HEIGHT - 1) * VGA_WIDTH * 2);
	memset(last_row, 0, VGA_WIDTH * 2);

	if (terminal_row > 0) terminal_row--;
	terminal_updatepos();
}

void terminal_putchar(char c) {
	if (c == '\n') {
		terminal_column = 0;
		if (++terminal_row == VGA_HEIGHT) terminal_pushrow();
		return;
	}

	terminal_putentryat(c, terminal_color, terminal_column, terminal_row);
	if (++terminal_column == VGA_WIDTH)
	{
		terminal_column = 0;
		if (++terminal_row == VGA_HEIGHT) terminal_pushrow();
	}
}

void terminal_putch(char c) {
	terminal_putchar(c);
	terminal_updatepos();
}

void terminal_write(const char* data, size_t size) {
	for (size_t i = 0; i < size; i++)
		terminal_putchar(data[i]);
	terminal_updatepos();
}

void terminal_writestring(const char* data) {
	terminal_write(data, strlen(data));
}
