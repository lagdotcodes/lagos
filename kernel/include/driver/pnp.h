// lagos: include/driver/pnp.h
#ifndef _DRIVER_PNP_H
#define _DRIVER_PNP_H

#define PNP_ADDRESS					0x0279
#define PNP_WRITE_DATA				0x0A79
#define PNP_READ_DATA_START			0x0203
#define PNP_READ_DATA_END			0x03FF

#endif
