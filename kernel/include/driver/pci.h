// lagos: include/driver/pci.h
#ifndef _DRIVER_PCI_H
#define _DRIVER_PCI_H

#include <stdint.h>

void pci_init();
u16 pci_config_read(u8 bus, u8 slot, u8 func, u8 offset);
u16 pci_check_vendor(u8 bus, u8 slot, u16* vendor, u16* device);

#endif
