// lagos: include/kernel/kkb.h
#ifndef _KERNEL_KKB_H
#define _KERNEL_KKB_H

#include <stdint.h>

typedef enum kkb_key {
	// Printable keys
	CHTYPE_PRINT,
	CHTYPE_PRINT_NUMPAD,
	CHTYPE_CONTROL,

	// Unprintable keys
	CHTYPE_UNPRINTABLE = 0x10,
	CHTYPE_SPECIAL,
	CHTYPE_FUNCTION,
	CHTYPE_SHIFT,
	CHTYPE_LOCK,
	CHTYPE_UNDEFINED
} kkb_key_t;

#define CHLOCK_CAPS			0x01
#define CHLOCK_SCROLL		0x02
#define CHLOCK_NUM			0x04

#define CHSHIFT_ALT			0x03
#define CHSHIFT_ALT_LEFT	0x01
#define CHSHIFT_ALT_RIGHT	0x02
#define CHSHIFT_SHIFT		0x0C
#define CHSHIFT_SHIFT_LEFT	0x04
#define CHSHIFT_SHIFT_RIGHT	0x08
#define CHSHIFT_CTRL		0x30
#define CHSHIFT_CTRL_LEFT	0x10
#define CHSHIFT_CTRL_RIGHT	0x20

#define CHSPEC_MEDIA		0x01
#define CHSPEC_WINKEY		0x02
#define CHSPEC_WINKEY2		0x03
#define CHSPEC_CONTEXT		0x04
#define CHSPEC_INSERT		0x10
#define CHSPEC_HOME			0x11
#define CHSPEC_PAGE_UP		0x12
#define CHSPEC_DELETE		0x13
#define CHSPEC_END			0x14
#define CHSPEC_PAGE_DOWN	0x15
#define CHSPEC_UP			0x20
#define CHSPEC_LEFT			0x21
#define CHSPEC_DOWN			0x22
#define CHSPEC_RIGHT		0x23

typedef struct kkb_ch {
	kkb_key_t type;
	ch value;
} kkb_ch_t;

void kkb_init();
u8 kkb_getch();
bool kkb_push(u8 scan);

#endif
