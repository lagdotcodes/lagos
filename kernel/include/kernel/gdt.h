// lagos: include/kernel/gdt.h
#ifndef _KERNEL_GDT_H
#define _KERNEL_GDT_H

typedef struct_packed(gdt_entry_struct) {
	unsigned short	limit_low;			// The lower 16 bits of the limit.
	unsigned short	base_low;			// The lower 16 bits of the base.
	unsigned char	base_mid;			// The next 8 bits of the base.
	unsigned char	access;				// Access flags, determine what ring this segment can be used in.
	unsigned char	granularity;
	unsigned char	base_high;			// The last 8 bits of the base.
} gdt_entry_t;

typedef struct_packed(gdt_ptr_struct) {
	unsigned short	limit;				// The upper 16 bits of all selector limits.
	unsigned int	base;				// The address of the first gdt_entry_t struct.
} gdt_ptr_t;

typedef struct_packed(idt_entry_struct) {
	unsigned short	base_lo;			// The lower 16 bits of the address to jump to when this interrupt fires.
	unsigned short	sel;				// Kernel segment selector.
	unsigned char	always0;			// This must always be zero.
	unsigned char	flags;				// More flags. See documentation.
	unsigned short	base_hi;			// The upper 16 bits of the address to jump to.
} idt_entry_t;

// A struct describing a pointer to an array of interrupt handlers.
// This is in a format suitable for giving to 'lidt'.
typedef struct_packed(idt_ptr_struct) {
	unsigned short	limit;
	unsigned int	base;				// The address of the first element in our idt_entry_t array.
} idt_ptr_t;

void init_descriptor_tables();

#define GDTA_CODE			0x01
#define GDTA_CONFORMING		0x02
#define GDTA_EXPANDDOWN		0x02
#define GDTA_READABLE		0x04
#define GDTA_WRITABLE		0x04
#define GDTA_ACCESSED		0x08
#define GDTA_APPLICATION	0x10
#define GDTA_RING0			0x00
#define GDTA_RING1			0x20
#define GDTA_RING2			0x40
#define GDTA_RING3			0x60
#define GDTA_PRIVILEGE		0x60
#define GDTA_PRESENT		0x80

#define GDTG_LONG			0x20
#define GDTG_32BIT			0x40
#define GDTG_SHIFT			0x80

#define GDT_BASE(g) (unsigned int)((g->base_high << 24) | (g->base_mid << 16) | g->base_low)
inline unsigned int gdt_len(gdt_entry_t* gdt) {
	unsigned int n = ((gdt->granularity & 0x0F) << 16) | gdt->limit_low;
	if (gdt->granularity & GDTG_SHIFT) n = (n << 12) + 0xFFF;
	return n;
}

void gdt_info();
void gdt_print(gdt_entry_t* gdt);

#endif
