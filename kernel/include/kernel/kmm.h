// lagos: include/kernel/kmm.h
#ifndef _KERNEL_KMM_H
#define _KERNEL_KMM_H

#include <stddef.h>
#include <stdint.h>
#include <kernel/multiboot.h>
#include <sys/cdefs.h>

void kmm_init(multiboot_info_t* mb);

void kmm_info();
ptr kmm_alloc(size_t n);
void kmm_free(ptr m);

#endif
