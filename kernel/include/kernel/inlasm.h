// lagos: include/kernel/inlasm.h
#ifndef _KERNEL_INLASM_H
#define _KERNEL_INLASM_H

#include <stdbool.h>
#include <stdint.h>
#include <sys/cdefs.h>

static inline u32 farpeekl(u16 sel, void* off) {
	u32 ret;
#ifdef __GNUC__
	asm("push %%fs\n\t"
		"mov  %1, %%fs\n\t"
		"mov  %%fs:(%2), %0\n\t"
		"pop  %%fs"
		: "=r"(ret) : "g"(sel), "r"(off) );
#elif defined(_MSC_VER)
	__asm {
		push fs;
		mov fs, sel;
		mov ret, fs:off;
		pop fs;
	};
#endif
	return ret;
}

static inline void farpokeb(u16 sel, void* off, u8 v) {
#ifdef __GNUC__
	asm("push %%fs\n\t"
		"mov  %0, %%fs\n\t"
		"movb %2, %%fs:(%1)\n\t"
		"pop %%fs"
		: : "g"(sel), "r"(off), "r"(v) );
	/* TODO: Should "memory" be in the clobber list here? */
#elif defined(_MSC_VER)
	__asm {
		push fs;
		mov fs, sel;
		mov fs:off, v;
		pop fs;
	};
#endif
}

static inline void outb(u16 port, u8 val) {
#ifdef __GNUC__
	asm volatile( "outb %0, %1" : : "a"(val), "Nd"(port) );
	/* TODO: Is it wrong to use 'N' for the port? It's not a 8-bit constant. */
	/* TODO: Should %1 be %w1? */
#elif defined(_MSC_VER)
	__asm {
		mov al, val;
		mov dx, port;
		out dx, al;
	};
#endif
}

static inline void outl(u16 port, u32 val) {
#ifdef __GNUC__
	asm volatile( "outl %0, %1" : : "a"(val), "Nd"(port) );
	/* TODO: Is it wrong to use 'N' for the port? It's not a 8-bit constant. */
	/* TODO: Should %1 be %w1? */
#elif defined(_MSC_VER)
	__asm {
		mov ax, val;
		mov dx, port;
		out dx, ax;
	};
#endif
}

static inline u8 inb(u16 port) {
	u8 ret;
#ifdef __GNUC__
	asm volatile( "inb %1, %0" : "=a"(ret) : "Nd"(port) );
	/* TODO: Is it wrong to use 'N' for the port? It's not a 8-bit constant. */
	/* TODO: Should %1 be %w1? */
#elif defined(_MSC_VER)
	__asm {
		mov dx, port;
		in dx;
		mov ret, al;
	};
#endif
	return ret;
}

static inline u32 inl(u16 port) {
	u32 ret;
#ifdef __GNUC__
	asm volatile( "inl %1, %0" : "=a"(ret) : "Nd"(port) );
	/* TODO: Is it wrong to use 'N' for the port? It's not a 8-bit constant. */
	/* TODO: Should %1 be %w1? */
#elif defined(_MSC_VER)
	__asm {
		mov dx, port;
		in dx;
		mov ret, ax;
	};
#endif
	return ret;
}

/* static inline void io_wait(void) {
	// TODO: This is probably fragile.
	asm volatile ( "jmp 1f\n\t"
				   "1:jmp 2f\n\t"
				   "2:" );
} */

static inline void io_wait(void) {
#ifdef __GNUC__
	// Port 0x80 is used for 'checkpoints' during POST.
	// The Linux kernel seems to think it is free for use :-/
	asm volatile ( "outb %%al, $0x80" : : "a"(0) );
	// TODO: Is there any reason why al is forced?
#elif defined(_MSC_VER)
	__asm {
		outb 0x80, al;
	};
#endif
}

static inline bool are_interrupts_enabled() {
	unsigned long flags;
#ifdef __GNUC__
	asm volatile("pushf\n\t"
				"pop %0"
				: "=g"(flags) );
#elif defined(_MSC_VER)
	__asm {
		pushf;
		pop flags;
	};
#endif
	return flags & (1 << 9);
}

static inline void lidt(void* base, u16 size) {
	struct_packed(idtr_reg_t) {
		u16 length;
		u32 base;
	} IDTR;
 
	IDTR.length = size;
	IDTR.base = (u32) base;
#ifdef __GNUC__
	asm( "lidt (%0)" : : "p"(&IDTR) );
#elif defined(_MSC_VER)
	__asm {
		lidt IDTR;
	};
#endif
}

static inline void cpuid(int code, u32* a, u32* d) {
#ifdef __GNUC__
	asm volatile( "cpuid" : "=a"(*a), "=d"(*d) : "a"(code) : "ebx", "ecx" );
#elif defined(_MSC_VER)
	__asm {
		cpuid;
		mov a, eax;
		mov d, edx;
	};
#endif
}

static inline int cpuid_string(int code, u32 where[4]) {
#ifdef __GNUC__
	asm volatile(	"cpuid" :
					"=a"(where), "=b"(*(where+1)), "=d"(*(where+2)), "=c"(*(where+3)) :
					"a"(code));
#elif defined(_MSC_VER)
	__asm {
		cpuid;
		mov where[0], eax;
		mov where[1], ebx;
		mov where[2], edx;
		mov where[3], ecx;
	};
#endif
	return (int)where[0];
}

static inline u64 rdtsc() {
	u64 ret;
#ifdef __GNUC__
	asm volatile( "rdtsc" : "=A"(ret) );
#elif defined(_MSC_VER)
	__asm {
		rdtsc;
		mov ret, eax;
		mov ret[4], edx;
	};
#endif
	return ret;
}

static inline unsigned long read_cr0(void) {
	unsigned long val;
#ifdef __GNUC__
	asm volatile( "mov %%cr0, %0" : "=r"(val) );
#elif defined(_MSC_VER)
	__asm {
		mov val, cr0;
	};
#endif
	return val;
}

static inline void invlpg(void* m) {
#ifdef __GNUC__
	/* Clobber memory to avoid optimizer re-ordering access before invlpg, which may cause nasty bugs. */
	asm volatile( "invlpg (%0)" : : "b"(m) : "memory" );
#elif defined(_MSC_VER)
	__asm {
		mov eax, m;
		invlpg;
	};
#endif
}

inline void wrmsr(u32 msr_id, u64 msr_value) {
#ifdef __GNUC__
	asm volatile( "wrmsr" : : "c" (msr_id), "A" (msr_value) );
#elif defined(_MSC_VER)
	__asm {
		mov eax, msr_value;
		mov edx, msr_value[4];
		mov ecx, msr_id;
		wrmsr;
	};
#endif
}

inline u64 rdmsr(u32 msr_id) {
	u64 msr_value;
#if __GNUC__
	asm volatile( "rdmsr" : "=A" (msr_value) : "c" (msr_id) );
#elif defined(_MSC_VER)
	__asm {
		mov ecx, msr_id;
		rdmsr;
		mov msr_value, eax;
		mov msr_value[4], edx;
	};
#endif
	return msr_value;
}

#endif
