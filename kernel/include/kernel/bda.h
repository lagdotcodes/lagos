// lagos: include/kernel/bda.h
#ifndef _KERNEL_BDA_H
#define _KERNEL_BDA_H

//#include <stdint.h>

#define HW_FLOPPY		0x0001
#define HW_MATH			0x0002
#define HW_MOUSE		0x0004
#define HW_VID			0x0030
#define HW_VID_EGA		0x0000
#define HW_VID_40_25	0x0010
#define HW_VID_80_25	0x0020
#define HW_VID_MONO		0x0030
#define HW_FLOPPIES		0x00C0
#define HW_SERIALS		0x0700
#define HW_PARA_1		0x0000
#define HW_PARA_2		0x8000
#define HW_PARA_3		0xC000

#define VA_TEST23		0x01
#define VA_GRAPHICS45	0x02
#define VA_MONO			0x04
#define VA_ENABLED		0x08
#define VA_MODE6		0x10
#define VA_BLINK		0x20

struct_packed(bios_data_area) {
	unsigned short com1;
	unsigned short com2;
	unsigned short com3;
	unsigned short com4;
	unsigned short lpt1;
	unsigned short lpt2;
	unsigned short lpt3;
	unsigned short ebda;
	unsigned short hardware;
	unsigned char intflag;
	unsigned short memsize;
	unsigned short adaptermemsize;
	unsigned short keyflags;
	unsigned char altnumpad;
	unsigned short keybuf_next;
	unsigned short keybuf_last;
	unsigned char keybuf[32];
	unsigned char floppy_calibration;
	unsigned char floppy_motor;
	unsigned char floppy_motor_timeout;
	unsigned char floppy_status;
	unsigned char hdfloppy_ctrl0;
	unsigned char hdfloppy_ctrl1;
	unsigned char hdfloppy_ctrl2;
	unsigned char floppy_cylinder;
	unsigned char floppy_head;
	unsigned char floppy_sector;
	unsigned char floppy_byte;
	unsigned char vidmode;
	unsigned short textcols;
	unsigned short vidpage_size;
	unsigned short vidpage_offset;
	unsigned short vidpage_cursor[8];
	unsigned short cursor_shape;
	unsigned char vidpage;
	unsigned short vidport;
	unsigned char vidattr;
	unsigned char vidpalette;
	unsigned short adapter_offset;
	unsigned short adapter_segment;
	unsigned char last_interrupt;
	unsigned int timer;
	unsigned char timer_24h;
	unsigned char ctrl_break;
	unsigned short soft_reset;
	unsigned char hd_op_status;
	unsigned char hd_count;
	unsigned char hd_control;
	unsigned char hdport;
	unsigned char para_timeout[4];
	unsigned char serial_timeout[4];
	unsigned short keybuf_start;
	unsigned short keybuf_end;
	unsigned char vid_rows;
	unsigned short scanlines_per_char;
	unsigned char vid_options;
	unsigned char vga_flags1;
	unsigned char vga_flags2;
	unsigned char floppy_config;
	unsigned char hd_status;
	unsigned char hd_error;
	unsigned char hd_complete;
	unsigned char floppy_info;
	unsigned char disk0_media_state;
	unsigned char disk1_media_state;
	unsigned char disk0_op_state;
	unsigned char disk1_op_state;
	unsigned char disk0_cylinder;
	unsigned char disk1_cylinder;
	unsigned char key_flags3;
	unsigned char key_flags4;
	unsigned int user_wait_address;
	unsigned int user_wait_count;
	unsigned char user_wait_flag;
	unsigned char lan[7];
	unsigned int vid_param_address;
	unsigned char reserved[68];
	unsigned char comms[16];
};

extern const struct bios_data_area* bda;
	
#endif
