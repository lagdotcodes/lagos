// lagos: include/kernel/kkb.h
#ifndef _KERNEL_KKB_H
#define _KERNEL_KKB_H

typedef struct kthread_ctx {
	u32 cs, ds, es, fs, gs, eip;
	u32 eflags;										// pushfd
	u32 edi, esi, ebp, esp, ebx, edx, ecx, eax;		// pushad
} kthread_ctx_t;

typedef struct kthread {
	str name, path;
	u32 id, parent;
	u8 priority, timer;
	kthread_ctx_t ctx;
} kthread_t;

#endif
