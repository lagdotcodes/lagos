// lagos: include/kernel/rsdp.h
#ifndef _KERNEL_RSDP_H
#define _KERNEL_RSDP_H

#include <stdint.h>

#define RSDP_SIGNATURE "RSD PTR "

typedef struct rsdp_descriptor {
	char signature[8];
	u8 checksum;
	char oem_id[6];
	u8 revision;
	u32 rsdt_address;
} rsdp_v1_t;

typedef struct rsdp_descriptor_v2 {
	rsdp_v1_t v1;
	u32 length;
	u64 xsdt_address;
	u8 extended_checksum;
	u8 reserved[3];
} rsdp_v2_t;

extern rsdp_v1_t* rsdp_v1;
extern rsdp_v2_t* rsdp_v2;

typedef struct isdt_header {
	char signature[4];
	u32 length;
	u8 revision;
	u8 checksum;
	char oem_id[6];
	char oem_table_id[8];
	u32 oem_revision;
	char creator_id[4];
	u32 creator_revision;
} isdt_t;

typedef struct rsdt_struc {
	isdt_t header;
	u32 tables[];
} rsdt_t;

typedef struct xsdt_struc {
	isdt_t header;
	u64 tables[];
} xsdt_t;

extern rsdt_t* rsdt;
extern xsdt_t* xsdt;

void rsdp_init();
void rsdp_info();

#endif
