// lagos: include/kernel/kmm.h
#ifndef _ARCH_I386_H
#define _ARCH_I386_H

// I/O Ports

// DMA1: Direct Memory Access controller 8237
#define DMA1_ADDR0					0x0000
#define DMA1_COUNT0					0x0001
#define DMA1_ADDR1					0x0002
#define DMA1_COUNT1					0x0003
#define DMA1_ADDR2					0x0004
#define DMA1_COUNT2					0x0005
#define DMA1_ADDR3					0x0006
#define DMA1_COUNT3					0x0007
#define DMA1_STATUS					0x0008
#define DMA1_COMMAND				0x0008
#define DMA1_WRITE_REQ				0x0009
#define DMA1_MASK					0x000A
#define DMA1_MODE					0x000B
#define DMA1_CLEAR_FLIP_FLOP		0x000C
#define DMA1_READ_TEMP				0x000D
#define DMA1_MASTER_CLEAR			0x000D
#define DMA1_CLEAR_MASK				0x000E
#define DMA1_WRITE_MASK				0x000F

#define DMA_STATUS_COUNT(n)			(1 << n)
#define DMA_STATUS_COUNT0			0x01
#define DMA_STATUS_COUNT1			0x02
#define DMA_STATUS_COUNT2			0x04
#define DMA_STATUS_COUNT3			0x08
#define DMA_STATUS_CH(n)			(1 << (n + 4))
#define DMA_STATUS_CH0				0x10
#define DMA_STATUS_CH1				0x20
#define DMA_STATUS_CH2				0x40
#define DMA_STATUS_CH3				0x80

#define DMA_CMD_MEM_TO_MEM			0x00
#define DMA_CMD_CONTROLLER			0x04
#define DMA_CMD_NORMAL_TIME			0x00
#define DMA_CMD_COMPRESSED_TIME		0x08
#define DMA_CMD_FIXED_PRI			0x00
#define DMA_CMD_ROTATING_PRI		0x10
#define DMA_CMD_LATE_WRITE			0x00
#define DMA_CMD_EXTENDED_WRITE		0x20
#define DMA_CMD_DREQ_LOW			0x00
#define DMA_CMD_DREQ_HIGH			0x40
#define DMA_CMD_DACK_LOW			0x00
#define DMA_CMD_DACK_HIGH			0x80

#define DMA_MASK_CH0				0x00
#define DMA_MASK_CH1				0x01
#define DMA_MASK_CH2				0x02
#define DMA_MASK_CH3				0x03
#define DMA_MASK_SET				0x04

#define DMA_MODE_CH0				0x00
#define DMA_MODE_CH1				0x01
#define DMA_MODE_CH2				0x02
#define DMA_MODE_CH3				0x03
#define DMA_MODE_VERIFY				0x00
#define DMA_MODE_WRITE				0x04
#define DMA_MODE_READ				0x08
#define DMA_MODE_INCREMENT			0x00
#define DMA_MODE_DECREMENT			0x20
#define DMA_MODE_DEMAND				0x00
#define DMA_MODE_SINGLE				0x40
#define DMA_MODE_BLOCK				0x80
#define DMA_MODE_CASCADE			0xC0


// DMA2: Direct Memory Access controller 8237
#define DMA2_ADDR0					0x0010
#define DMA2_COUNT0					0x0011
#define DMA2_ADDR1					0x0012
#define DMA2_COUNT1					0x0013
#define DMA2_ADDR2					0x0014
#define DMA2_COUNT2					0x0015
#define DMA2_ADDR3					0x0016
#define DMA2_COUNT3					0x0017
#define DMA2_STATUS					0x0018
#define DMA2_COMMAND				0x0018
#define DMA2_WRITE_REQ				0x0019
#define DMA2_MASK					0x001A
#define DMA2_MODE					0x001B
#define DMA2_CLEAR_FLIP_FLOP		0x001C
#define DMA2_READ_TEMP				0x001D
#define DMA2_MASTER_CLEAR			0x001D
#define DMA2_CLEAR_MASK				0x001E
#define DMA2_WRITE_MASK				0x001F


// PS/2
#define PS2_EXTENDED_FUNC			0x0018
#define PS2_EXTENDED_EXEC			0x001A


// PIC1: Programmable Interrupt Controller 8259
#define PIC1_ICW1					0x0020
#define PIC1_INTERRUPT_REQ			0x0020
#define PIC1_OCW2					0x0020
#define PIC1_OCW3					0x0020
#define PIC1_ICW2					0x0021
#define PIC1_ICW3					0x0021
#define PIC1_ICW4					0x0021
#define PIC1_MASK					0x0021

#define PIC1_ICW1_NOICW4			0x00
#define PIC1_ICW1_ICW4				0x01
#define PIC1_ICW1_CASCADE			0x00
#define PIC1_ICW1_SINGLE			0x02
#define PIC1_ICW1_8BYTES			0x00
#define PIC1_ICW1_4BYTES			0x04
#define PIC1_ICW1_EDGE_TRIGGER		0x00
#define PIC1_ICW1_LEVEL_TRIGGER		0x08
#define PIC1_ICW1_ISSUED			0x10

#define PIC1_ICW2_ADDRESS(x)		(x << 3)
#define PIC1_ICW3_SLAVE(x)			(1 << x)
#define PIC1_ICW4_8085				0x00
#define PIC1_ICW4_8086				0x01
#define PIC1_ICW4_8088				0x01
#define PIC1_ICW4_NORMAL_EOI		0x00
#define PIC1_ICW4_AUTO_EOI			0x02
#define PIC1_ICW4_NONBUFFERED		0x00
#define PIC1_ICW4_BUFFER_SLAVE		0x08
#define PIC1_ICW4_BUFFER_MASTER		0x0C
#define PIC1_ICW4_FULLY_NESTED		0x10

#define PIC1_MASK_TIMER				0x01
#define PIC1_MASK_KEYBOARD			0x02
#define PIC1_MASK_VIDEO				0x04
#define PIC1_MASK_SERIAL2			0x08
#define PIC1_MASK_SERIAL1			0x10
#define PIC1_MASK_FIXED_DISK		0x20
#define PIC1_MASK_DISKETTE			0x40
#define PIC1_MASK_PARELLEL_PRINTER	0x80

#define PIC1_OCW2_INT(x)			(x & 0x07)
#define PIC1_OCW2_ROT_CLEAR			0x00
#define PIC1_OCW2_NONSPECIFIC_EOI	0x20
#define PIC1_OCW2_NOOP				0x40
#define PIC1_OCW2_SPECIFIC_EOI		0x60
#define PIC1_OCW2_ROT_SET			0x80
#define PIC1_OCW2_ROT_NONSPECIFIC	0xA0
#define PIC1_OCW2_SET_PRIORITY		0xC0
#define PIC1_OCW2_ROT_SPECIFIC		0xE0

#define PIC1_OCW3_READ_REQ			0x02
#define PIC1_OCW3_READ_ISR			0x03
#define PIC1_OCW3_POLL				0x04
#define PIC1_OCW3_RESET_MASK		0x40
#define PIC1_OCW3_SET_MASK			0x60


// PIT: Programmable Interrupt Timer 8253/8254
#define PIT_C1						0x0040
#define PIT_DIVISOR					0x0040
#define PIT_C2						0x0041
#define PIT_RAM_REFRESH				0x0041
#define PIT_C3						0x0042
#define PIT_CASETTE					0x0042
#define PIT_MODE					0x0043
#define PIT_CONTROL012				0x0043
#define PIT_C3						0x0044
#define PIT_CONTROL3				0x0047
#define PIT_EISA					0x0048
#define PIT_EISA_TIMER2				0x004A

#define PIT_CTRL_BINARY16			0x00
#define PIT_CTRL_BCD				0x01
#define PIT_CTRL_ONE_SHOT			0x02
#define PIT_CTRL_RATE				0x04
#define PIT_CTRL_SQUARE_WAVE		0x05
#define PIT_CTRL_SOFTWARE_STROBE	0x06
#define PIT_CTRL_HARDWARE_STROBE	0x07
#define PIT_CTRL_LATCH				0x00
#define PIT_CTRL_BYTE1				0x10
#define PIT_CTRL_BYTE2				0x20
#define PIT_CTRL_BYTE1_2			0x30
#define PIT_CTRL_C0					0x00
#define PIT_CTRL_C1					0x40
#define PIT_CTRL_C2					0x80
#define PIT_CTRL_C3					0x00


#endif
