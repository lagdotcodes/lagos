// lagos: include/driver/pci.c
#include <driver/pci.h>
#include <kernel/inlasm.h>
#include <stddef.h>
#include <stdio.h>

#define PCI_CONFIG_ADDRESS				0x0CF8
#define PCI_CONFIG_DATA					0x0CFC

#define PCI_ENABLE						0x80000000
#define PCIREG(n)						(u32)(n & 0xFC)
#define PCIFUN(n)						(u32)(n << 8)
#define PCIDEV(n)						(u32)(n << 11)
#define PCIBUS(n)						(u32)(n << 16)

#define PCIHDR_STANDARD					0x00
#define PCIHDR_BRIDGE					0x01
#define PCIHDR_CARDBUS					0x02

#define PCICL_UNKNOWN					0x00
#define PCICL_MASS_STORAGE				0x01
#define PCICL_NETWORK					0x02
#define PCICL_DISPLAY					0x03
#define PCICL_MULTIMEDIA				0x04
#define PCICL_MEMORY					0x05
#define PCICL_BRIDGE					0x06
#define PCICL_COMMS						0x07
#define PCICL_SYSTEM					0x08
#define PCICL_INPUT						0x09
#define PCICL_DOCKING_STATION			0x0A
#define PCICL_PROCESSOR					0x0B
#define PCICL_SERIAL_BUS				0x0C
#define PCICL_WIRELESS					0x0D
#define PCICL_INTELLIGENT				0x0E
#define PCICL_SATELLITE					0x0F
#define PCICL_CRYPT						0x10
#define PCICL_SIGNAL_PROCESSING			0x11
#define PCICL_ACCELERATOR				0x12
#define PCICL_NONESSENTIAL				0x13
#define PCICL_UNASSIGNED				0xFF

#define PCIBRIDGE_PCI2PCI				0x04

typedef struct {
	u32 bar0;
	u32 bar1;
	u32 bar2;
	u32 bar3;
	u32 bar4;
	u32 bar5;
	u32 cardbus_cis;
	u16 subsystem_vendor, subsystem;
	u32 expansion_rom;
	u8 caps;
	u8 reserved0;
	u16 reserved1;
	u32 reserved2;
	u8 interrupt_line, interrupt_pin, min_grant, max_latency;
} pcistandard_t;

typedef struct {
	u32 bar0;
	u32 bar1;
	u8 primary_bus_num, secondary_bus_num, subord_bus_num, secondary_latency_timer;
	u8 io_base, io_limit;
	u16 secondary_status;
	u16 mem_base, mem_limit;
	u16 prefetch_base, prefetch_limit;
	u32 prefetch_base_upper;
	u32 prefetch_limit_upper;
	u16 io_base_upper, io_limit_upper;
	u8 caps, reserved0;
	u16 reserved1;
	u32 expansion_rom;
	u8 interrupt_line, interrupt_pin;
	u16 bridge_control;
} pcibridge_t;

typedef struct {
	u32 cardbus_address;
	u8 caps, reserved0;
	u16 secondary_status;
	u8 pci_bus_num, cardbus_bus_num, subord_bus_num, cardbus_latency_timer;
	u32 mem0_base;
	u32 mem0_limit;
	u32 mem1_base;
	u32 mem1_limit;
	u32 io0_base;
	u32 io0_limit;
	u32 io1_base;
	u32 io1_limit;
	u8 interrupt_line, interrupt_pin;
	u16 bridge_control;
	u16 subsystem, subsystem_vendor;
	u32 legacy_address;
} pcicardbus_t;

typedef struct {
	u16 vendor, device;
	u16 command, status;
	u8 revision, prog_if, subclass, class;
	u8 cache_line_size, latency_timer, header_type, bist;
	union {
		pcistandard_t standard;
		pcibridge_t pci2pci;
		pcicardbus_t cardbus;
	};
} pciheader_t;

u16 pci_config_read(u8 bus, u8 slot, u8 func, u8 offset)
 {
    u32 address;
    u32 lbus  = (u32)bus;
    u32 lslot = (u32)slot;
    u32 lfunc = (u32)func;
    u16 tmp = 0;
 
    /* create configuration address as per Figure 1 */
    address = (u32)((lbus << 16) | (lslot << 11) |
              (lfunc << 8) | (offset & 0xfc) | ((u32)0x80000000));
 
    /* write out the address */
    outl(0xCF8, address);
    /* read in the data */
    /* (offset & 2) * 8) = 0 will choose the first word of the 32 bits register */
    tmp = (u16)((inl(0xCFC) >> ((offset & 2) * 8)) & 0xffff);
    return (tmp);
 }

bool pci_read_device(pciheader_t* dev, u8 bus, u8 slot, u8 func) {
	u16* devptr = (u16*)dev;
	u8 limit = sizeof(pciheader_t);
	for (u8 offset = 0; offset < limit; offset += 2) {
		devptr[offset / 2] = pci_config_read(bus, slot, func, offset);
	}
	return true;
}

typedef struct {
	u8 class, subclass, prog_if, any;
	char describe[24];
} pciclass_t;

typedef struct {
	u16 vendor, device;
	u8 any;
	char describe[24];
} pcidev_t;

pciclass_t pci_classes[] = {
	{0x00, 0x00, 0x00, 0, "??? unknown"},
	{0x00, 0x01, 0x00, 0, "??? vga"},
	
	{0x01, 0x00, 0x00, 0, "storage scsi"},
	{0x01, 0x01, 0x00, 0, "storage ide-1 op"},
	{0x01, 0x01, 0x01, 0, "storage ide-1 prog"},
	{0x01, 0x01, 0x02, 0, "storage ide-2 op"},
	{0x01, 0x01, 0x03, 0, "storage ide-2 prog"},
	{0x01, 0x01, 0x07, 0, "storage ide-master"},
	{0x01, 0x02, 0x00, 0, "storage floppy"},
	{0x01, 0x03, 0x00, 0, "storage ipi"},
	{0x01, 0x04, 0x00, 0, "storage raid"},
	{0x01, 0x05, 0x20, 0, "storage ata single"},
	{0x01, 0x05, 0x30, 0, "storage ata chained"},
	{0x01, 0x06, 0x00, 0, "storage sata vendor"},
	{0x01, 0x06, 0x01, 0, "storage sata ahci-1.0"},
	{0x01, 0x07, 0x00, 0, "storage sas"},
	{0x01, 0x80, 0x00, 0, "storage ???"},
	
	{0x02, 0x00, 0x00, 0, "net ethernet"},
	{0x02, 0x01, 0x00, 0, "net token-ring"},
	{0x02, 0x02, 0x00, 0, "net fddi"},
	{0x02, 0x03, 0x00, 0, "net atm"},
	{0x02, 0x04, 0x00, 0, "net isdn"},
	{0x02, 0x05, 0x00, 0, "net worldfip"},
	{0x02, 0x06, 0x00, 1, "net picmg-2.14"},
	{0x02, 0x80, 0x00, 0, "net other"},
	
	{0x03, 0x00, 0x00, 0, "vid vga"},
	{0x03, 0x00, 0x01, 0, "vid 8514"},
	{0x03, 0x01, 0x00, 0, "vid xga"},
	{0x03, 0x02, 0x00, 0, "vid 3d-non-vga"},
	{0x03, 0x80, 0x00, 0, "vid other"},
	
	{0x04, 0x00, 0x00, 0, "media video"},
	{0x04, 0x01, 0x00, 0, "media audio"},
	{0x04, 0x02, 0x00, 0, "media telephony"},
	{0x04, 0x80, 0x00, 0, "media other"},
	
	{0x05, 0x00, 0x00, 0, "mem ram"},
	{0x05, 0x01, 0x00, 0, "mem flash"},
	{0x05, 0x80, 0x00, 0, "mem other"},
	
	{0x06, 0x00, 0x00, 0, "bridge host"},
	{0x06, 0x01, 0x00, 0, "bridge isa"},
	{0x06, 0x02, 0x00, 0, "bridge eisa"},
	{0x06, 0x03, 0x00, 0, "bridge mca"},
	{0x06, 0x04, 0x00, 0, "bridge pci2pci"},
	{0x06, 0x04, 0x01, 0, "bridge pci2pci-sub"},
	{0x06, 0x05, 0x00, 0, "bridge pcmcia"},
	{0x06, 0x06, 0x00, 0, "bridge nubus"},
	{0x06, 0x07, 0x00, 0, "bridge cardbus"},
	{0x06, 0x08, 0x00, 1, "bridge raceway"},
	{0x06, 0x09, 0x40, 0, "bridge pci2pci-semi-pri"},
	{0x06, 0x09, 0x80, 0, "bridge pci2pci-semi-sec"},
	{0x06, 0x0A, 0x00, 0, "bridge infinibrand2pci"},
	{0x06, 0x80, 0x00, 0, "bridge other"},
	
	{0x07, 0x00, 0x00, 0, "serial xt"},
	{0x07, 0x00, 0x01, 0, "serial 16450"},
	{0x07, 0x00, 0x02, 0, "serial 16550"},
	{0x07, 0x00, 0x03, 0, "serial 16650"},
	{0x07, 0x00, 0x04, 0, "serial 16750"},
	{0x07, 0x00, 0x05, 0, "serial 16850"},
	{0x07, 0x00, 0x06, 0, "serial 16950"},
	{0x07, 0x01, 0x00, 0, "parallel"},
	{0x07, 0x01, 0x01, 0, "parallel bi"},
	{0x07, 0x01, 0x02, 0, "parallel ecp-1.x"},
	{0x07, 0x01, 0x03, 0, "parallel ieee1284-con"},
	{0x07, 0x01, 0xFE, 0, "parallel ieee1284-dev"},
	{0x07, 0x02, 0x00, 0, "serial multiport"},
	{0x07, 0x03, 0x00, 0, "modem"},
	{0x07, 0x03, 0x01, 0, "modem 16450"},
	{0x07, 0x03, 0x02, 0, "modem 16550"},
	{0x07, 0x03, 0x03, 0, "modem 16650"},
	{0x07, 0x03, 0x04, 0, "modem 16750"},
	{0x07, 0x04, 0x00, 0, "iee488-gpib"},
	{0x07, 0x05, 0x00, 0, "smartcard"},
	{0x07, 0x80, 0x00, 0, "comms other"},
	
	{0x08, 0x00, 0x00, 0, "pic 8259"},
	{0x08, 0x00, 0x01, 0, "pic isa"},
	{0x08, 0x00, 0x02, 0, "pic eisa"},
	{0x08, 0x00, 0x10, 0, "pic io"},
	{0x08, 0x00, 0x20, 0, "pic io-x"},
	{0x08, 0x01, 0x00, 0, "dma 8237"},
	{0x08, 0x01, 0x01, 0, "dma isa-dma"},
	{0x08, 0x01, 0x02, 0, "dma eisa"},
	{0x08, 0x02, 0x00, 0, "timer 8254"},
	{0x08, 0x02, 0x01, 0, "timer isa"},
	{0x08, 0x02, 0x02, 0, "timer eisa"},
	{0x08, 0x03, 0x00, 0, "rtc"},
	{0x08, 0x03, 0x01, 0, "rtc isa"},
	{0x08, 0x04, 0x00, 0, "pci hot-plug"},
	{0x08, 0x80, 0x00, 0, "system other"},
	
	{0x09, 0x00, 0x00, 0, "keyboard"},
	{0x09, 0x01, 0x00, 0, "digitizer"},
	{0x09, 0x02, 0x00, 0, "mouse"},
	{0x09, 0x03, 0x00, 0, "scanner"},
	{0x09, 0x04, 0x00, 0, "gameport"},
	{0x09, 0x04, 0x10, 0, "gameport legacy"},
	{0x09, 0x80, 0x00, 0, "input other"},

	{0x0A, 0x00, 0x00, 0, "docking"},
	{0x0A, 0x80, 0x00, 0, "docking other"},

	{0x0B, 0x00, 0x00, 0, "386"},
	{0x0B, 0x01, 0x00, 0, "486"},
	{0x0B, 0x02, 0x00, 0, "pentium"},
	{0x0B, 0x10, 0x00, 0, "alpha"},
	{0x0B, 0x20, 0x00, 0, "powerpc"},
	{0x0B, 0x30, 0x00, 0, "mips"},
	{0x0B, 0x40, 0x00, 0, "copro"},

	{0x0C, 0x00, 0x00, 0, "ieee1394 firewire"},
	{0x0C, 0x00, 0x10, 0, "ieee1394 openhci"},
	{0x0C, 0x01, 0x00, 0, "access-bus"},
	{0x0C, 0x02, 0x00, 0, "ssa"},
	{0x0C, 0x03, 0x00, 0, "usb universal"},
	{0x0C, 0x03, 0x10, 0, "usb open"},
	{0x0C, 0x03, 0x20, 0, "usb 2-host"},
	{0x0C, 0x03, 0x80, 0, "usb"},
	{0x0C, 0x03, 0xFE, 0, "usb nothost"},
	{0x0C, 0x04, 0x00, 0, "fibre-channel"},
	{0x0C, 0x05, 0x00, 0, "smbus"},
	{0x0C, 0x06, 0x00, 0, "infiniband"},
	{0x0C, 0x07, 0x00, 0, "ipmi smic"},
	{0x0C, 0x07, 0x01, 0, "ipmi kybd"},
	{0x0C, 0x07, 0x02, 0, "ipmi block"},
	{0x0C, 0x08, 0x00, 0, "sercos"},
	{0x0C, 0x09, 0x00, 0, "canbus"},

	{0x0D, 0x00, 0x00, 0, "wireless irda"},
	{0x0D, 0x01, 0x00, 0, "wireless ir"},
	{0x0D, 0x10, 0x00, 0, "wireless rf"},
	{0x0D, 0x11, 0x00, 0, "wireless bluetooth"},
	{0x0D, 0x12, 0x00, 0, "wireless broadband"},
	{0x0D, 0x20, 0x00, 0, "wireless 802.11a"},
	{0x0D, 0x21, 0x00, 0, "wireless 802.11b"},
	{0x0D, 0x80, 0x00, 0, "wireless other"},
	
	{0x0E, 0x00, 0x00, 0, "io msg-fifo"},
	{0x0E, 0x00, 0x00, 1, "io i20-arch"},
	
	{0x0F, 0x01, 0x00, 0, "sat tv"},
	{0x0F, 0x02, 0x00, 0, "sat audio"},
	{0x0F, 0x03, 0x00, 0, "sat voice"},
	{0x0F, 0x04, 0x00, 0, "sat data"},

	{0x10, 0x00, 0x00, 0, "crypt net"},
	{0x10, 0x00, 0x00, 0, "crypt entertainment"},
	{0x10, 0x00, 0x00, 0, "crypt other"},

	{0x11, 0x00, 0x00, 0, "data dpio"},
	{0x11, 0x01, 0x00, 0, "data perf"},
	{0x11, 0x10, 0x00, 0, "data synch-freq"},
	{0x11, 0x20, 0x00, 0, "data manage"},
	{0x11, 0x80, 0x00, 0, "data other"},
	
	{0xFF, 0x00, 0x00, 0, "uncategorisable"},
};

pcidev_t pci_devices[] = {
	{0x0E11, 0x3033, 0, "Compaq 1280"},
	{0x0E11, 0x4000, 0, "Compaq Triflex"},
	{0x0E11, 0xAE10, 0, "Compaq Smart2p"},
	{0x0E11, 0xAE32, 0, "Compaq Netel100"},
	{0x0E11, 0xAE34, 0, "Compaq Netel10"},
	{0x0E11, 0xAE35, 0, "Compaq Netflex3i"},
	{0x0E11, 0xAE40, 0, "Compaq Netel100d"},
	{0x0E11, 0xAE43, 0, "Compaq Netel100pi"},
	{0x0E11, 0xB011, 0, "Compaq Netal100i"},
	{0x0E11, 0xF130, 0, "Compaq Thunder"},
	{0x0E11, 0xF150, 0, "Compaq Netflex38"},
	{0x0E11, 0x0000, 1, "Compaq"},
	
	{0x1000, 0x0000, 1, "NCR"},

	{0x1002, 0x0000, 1, "ATi"},

	{0x1004, 0x0000, 1, "VLSI"},
	
	{0x1005, 0x0000, 1, "ADL"},

	{0x100B, 0x0000, 1, "NS"},

	{0x100C, 0x0000, 1, "Tseng"},

	{0x100E, 0x0000, 1, "Weitek"},
	
	{0x1011, 0x0000, 1, "DEC"},

	{0x1013, 0x0000, 1, "Cirrus"},

	{0x1014, 0x0000, 1, "IBM"},

	{0x101C, 0x0000, 1, "WD"},

	{0x1022, 0x0000, 1, "AMD"},
	
	{0x1023, 0x0000, 1, "Trident"},
	
	{0x1025, 0x0000, 1, "AI"},

	{0x102B, 0x0000, 1, "Matrox"},

	{0x102C, 0x0000, 1, "CT"},

	{0x1031, 0x0000, 1, "Miro"},
	
	{0x1033, 0x0000, 1, "NEC"},
	
	{0x1036, 0x0000, 1, "FD"},
	
	{0x1039, 0x0000, 1, "SI"},
	
	{0x103C, 0x0000, 1, "HP"},
	
	{0x1042, 0x0000, 1, "PCTech"},
	
	{0x1045, 0x0000, 1, "Opti"},
	
	{0x104A, 0x0000, 1, "SGS"},
	
	{0x104B, 0x0000, 1, "BusLogic"},
	
	{0x104C, 0x0000, 1, "TI"},
	
	{0x104E, 0x0000, 1, "Oak"},
	
	{0x1050, 0x0000, 1, "Winbond"},
	
	{0x1047, 0x0000, 1, "Motorola"},
	
	{0x105A, 0x0000, 1, "Promise"},
	
	{0x1060, 0x0000, 1, "UMC"},
	
	{0x1061, 0x0000, 1, "X"},
	
	{0x1066, 0x0000, 1, "Picop"},
	
	{0x106B, 0x0000, 1, "Apple"},
	
	{0x1074, 0x0000, 1, "Nexgen"},
	
	{0x1077, 0x0000, 1, "QLogic"},
	
	{0x1078, 0x0000, 1, "Cyrix"},
	
	{0x107D, 0x0000, 1, "Leadtek"},
	
	{0x1080, 0x0000, 1, "Contaq"},
	
	{0x1083, 0x0000, 1, "Forex"},
	
	{0x108D, 0x0000, 1, "Olicom"},
	
	{0x108E, 0x0000, 1, "Sun"},
	
	{0x1095, 0x0000, 1, "CMD"},
	
	{0x1098, 0x0000, 1, "Vision"},
	
	{0x109E, 0x0000, 1, "Brooktree"},
	
	{0x10A8, 0x0000, 1, "Sierra"},
	
	{0x10AA, 0x0000, 1, "ACC"},
	
	{0x10AD, 0x0000, 1, "Winbond"},
	
	{0x10B3, 0x0000, 1, "Databook"},
	
	{0x10B5, 0x0000, 1, "PLX"},
	
	{0x10B6, 0x0000, 1, "Madge"},
	
	{0x10B7, 0x0000, 1, "3COM"},
	
	{0x10B8, 0x0000, 1, "SMC"},
	
	{0x10B9, 0x0000, 1, "AL"},
	
	{0x10BA, 0x0000, 1, "Mitsubishi"},
	
	{0x10BD, 0x0000, 1, "Surecom"},
	
	{0x10C8, 0x0000, 1, "Neomagic"},
	
	{0x10CD, 0x0000, 1, "ASP"},
	
	{0x10D9, 0x0000, 1, "Macronix"},
	
	{0x10DC, 0x0000, 1, "CERN"},
	
	{0x10DE, 0x0000, 1, "nVidia"},
	
	{0x10E0, 0x0000, 1, "IMS"},
	
	{0x10E1, 0x0000, 1, "Tekram"},
	
	{0x10E3, 0x0000, 1, "Tundra"},
	
	{0x10E8, 0x0000, 1, "AMCC"},
	
	{0x10EA, 0x0000, 1, "Interg"},
	
	{0x10EC, 0x0000, 1, "Realtek"},
	
	{0x10FA, 0x0000, 1, "TrueVision"},
	
	{0x1101, 0x0000, 1, "Init"},
	
	{0x1103, 0x0000, 1, "TTI"},
	
	{0x1106, 0x0000, 1, "VIA"},
	
	{0x1113, 0x0000, 1, "SMC"},
	
	{0x1119, 0x0000, 1, "Vortex"},
	
	{0x111A, 0x0000, 1, "EF"},
	
	{0x1127, 0x0000, 1, "FORE"},
	
	{0x112F, 0x0000, 1, "ImagingTech"},
	
	{0x1131, 0x0000, 1, "Philips"},
	
	{0x113C, 0x0000, 1, "Cyclone"},
	
	{0x1142, 0x0000, 1, "Alliance"},
	
	{0x1148, 0x0000, 1, "SK"},
	
	{0x114A, 0x0000, 1, "VMIC"},
	
	{0x114F, 0x0000, 1, "Digi"},
	
	{0x1159, 0x0000, 1, "Mutech"},
	
	{0x1163, 0x0000, 1, "Rendition"},
	
	{0x1179, 0x0000, 1, "Toshiba"},
	
	{0x1180, 0x0000, 1, "Ricoh"},
	
	{0x1191, 0x0000, 1, "Artop"},
	
	{0x1193, 0x0000, 1, "Zeitnet"},
	
	{0x119B, 0x0000, 1, "Omega"},
	
	{0x11AD, 0x0000, 1, "Liteon"},
	
	{0x11BC, 0x0000, 1, "NP"},
	
	{0x11C1, 0x0000, 1, "AT&T"},
	
	{0x11CB, 0x0000, 1, "Specialix"},
	
	{0x11D1, 0x0000, 1, "Auravision"},
	
	{0x11D5, 0x0000, 1, "Ikon"},
	
	{0x11DE, 0x0000, 1, "Zoran"},
	
	{0x11F4, 0x0000, 1, "Kinetic"},
	
	{0x11F6, 0x0000, 1, "Compex"},
	
	{0x11FE, 0x0000, 1, "RP"},
	
	{0x120E, 0x0000, 1, "Cyclades"},
	
	{0x120F, 0x0000, 1, "Essential"},
	
	{0x1217, 0x0000, 1, "O2"},
	
	{0x121A, 0x0000, 1, "3dfx"},
	
	{0x1236, 0x0000, 1, "SigmaDES"},
	
	{0x123F, 0x0000, 1, "CCube"},
	
	{0x1244, 0x0000, 1, "AVM"},
	
	{0x1246, 0x0000, 1, "Dipix"},
	
	{0x124D, 0x0000, 1, "Stallion"},
	
	{0x1255, 0x0000, 1, "Optibase"},
	
	{0x1267, 0x0000, 1, "Satsagem"},
	
	{0x1273, 0x0000, 1, "Hughes"},
	
	{0x1274, 0x0000, 1, "Ensoniq"},
	
	{0x12AE, 0x0000, 1, "Alteon"},
	
	{0x12C5, 0x0000, 1, "Picturel"},
	
	{0x12D2, 0x0000, 1, "nVidia SGS"},
	
	{0x1307, 0x0000, 1, "CBoards"},
	
	{0x1385, 0x0000, 1, "Netgear"},
	
	{0x1C1C, 0x0000, 1, "Symphony"},
	
	{0x1DE1, 0x0000, 1, "Tekram"},
	
	{0x3D3D, 0x0000, 1, "3DLabs"},
	
	{0x4005, 0x0000, 1, "Avance"},
	
	{0x4A14, 0x0000, 1, "Netvin"},
	
	{0x5333, 0x0000, 1, "S3"},
	
	{0x6666, 0x0000, 1, "DCI"},
	
	{0x8086, 0x1237, 0, "Intel 82441"},
	{0x8086, 0x7000, 0, "Intel 82371sb.1"},
	{0x8086, 0x7010, 0, "Intel 82371sb.2"},
	{0x8086, 0x7020, 0, "Intel 82371sb.3"},
	{0x8086, 0x7113, 0, "Intel 82371ab.3"},
	{0x8086, 0x0000, 1, "Intel"},
	
	{0x8E2E, 0x0000, 1, "KTI"},
	
	{0x9004, 0x0000, 1, "Adaptec"},
	
	{0x9005, 0x0000, 1, "Adaptec"},
	
	{0x907F, 0x0000, 1, "Atronics"},
	
	{0x9412, 0x0000, 1, "Holtek"},
	
	{0xE159, 0x0000, 1, "Tigerjet"},
	
	{0xEDD8, 0x0000, 1, "Ark"},
	
	{0xFFFF, 0xFFFF, 1, "???"},
};

void pci_describe(pciheader_t* dev) {
	pcidev_t* dd = NULL;
	size_t index = 0;
	while (pci_devices[index].vendor != 0xFFFF) {
		dd = &pci_devices[index++];
		if (dev->vendor == dd->vendor && (dd->any || dev->device == dd->device)) break;
	}
	if (dd->vendor != 0xFFFF) printf("%s ", dd->describe);
	else printf("v%x.d%x ", dev->vendor, dev->device);
	
	pciclass_t* cls = NULL;
	index = 0;
	while (pci_classes[index].class != PCICL_UNASSIGNED) {
		cls = &pci_classes[index++];
		if (dev->class == cls->class && dev->subclass == cls->subclass && (cls->any || dev->prog_if == cls->prog_if)) break;
	}
	if (cls->class != 0xFF) printf("%s\n", cls->describe);
	else printf("%d.%d.%d\n", dev->class, dev->subclass, dev->prog_if);
}

u16 pci_vendor_id(u8 bus, u8 slot, u8 func) { return pci_config_read(bus, slot, func, 0); }
u8 pci_header_type(u8 bus, u8 slot, u8 func) { return pci_config_read(bus, slot, func, 14) & 0xFF; }

void pci_check_function(u8 bus, u8 slot, u8 func) {
	pciheader_t dev;
	pci_read_device(&dev, bus, slot, func);
	
	printf("%d.%d.%d r%d ", bus, slot, func, dev.revision);
	pci_describe(&dev);
	
	if (dev.class == PCICL_BRIDGE && dev.subclass == PCIBRIDGE_PCI2PCI) {
		// scan secondary bus...
	}
}

void pci_check_device(u8 bus, u8 slot) {
	for (u8 func = 0; func < 8; func++) {
		u16 vendor = pci_vendor_id(bus, slot, func);
		if (vendor != 0xFFFF) pci_check_function(bus, slot, func);
	}
	
	/* DOING IT PROPERLY LOL
	u16 vendor = pci_vendor_id(bus, slot, 0);
	if (vendor == 0xFFFF) return;
	
	pci_check_function(bus, slot, 0);
	u8 header = pci_header_type(bus, slot, 0);
	if (header & 0x80) {
		for (u8 func = 1; func < 8; func++) {
			if (pci_vendor_id(bus, slot, func) != 0xFFFF) pci_check_function(bus, slot, func);
		}
	} */
}

void pci_init() {
	for (u16 bus = 0; bus < 256; bus++) {
		for (u8 slot = 0; slot < 32; slot++) {
			pci_check_device((u8)bus, slot);
		}
	}
}
