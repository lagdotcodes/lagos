// lagos: kernel/kkb.c
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <kernel/inlasm.h>
#include <kernel/kkb.h>

#define I8042_DATA					0x60
#define I8042_STATUS				0x64

#define KBSTATS_OBF					0x01
#define KBSTATS_IBF					0x02
#define KBSTATS_SYSTEM				0x04
#define KBSTATS_CMD_DATA			0x08
#define KBSTATS_LOCKED				0x10
#define KBSTATS_AUX_BUF				0x20
#define KBSTATS_TIMEOUT				0x40
#define KBSTATS_PARITY				0x80

typedef enum kbenc {
	KBENC_CMD_SET_LED = 0xED,
	KBENC_CMD_ECHO = 0xEE,
	KBENC_CMD_ALT_SCAN = 0xF0,
	KBENC_CMD_GET_ID = 0xF2,
	KBENC_CMD_SET_AUTOREPEAT = 0xF3,
	KBENC_CMD_ENABLE = 0xF4,
	KBENC_CMD_RESET = 0xF5,
	KBENC_CMD_RESET_SCAN = 0xF6,
	KBENC_CMD_ALL_A = 0xF7,
	KBENC_CMD_ALL_MB = 0xF8,
	KBENC_CMD_ALL_M = 0xF9,
	KBENC_CMD_ALL_AMB = 0xFA,
	KBENC_CMD_SINGLE_A = 0xFB,
	KBENC_CMD_SINGLE_MB = 0xFC,
	KBENC_CMD_SINGLE_B = 0xFD,
	KBENC_CMD_RESEND = 0xFE,
	KBENC_CMD_RESET_TEST = 0xFF
} kbenc_t;

typedef enum kbctrl {
	KBCTRL_CMD_READCMD = 0x20,
	KBCTRL_CMD_WRITECMD = 0x60,
	KBCTRL_CMD_DISABLE_MOUSE = 0xA7,
	KBCTRL_CMD_ENABLE_MOUSE = 0xA8,
	KBCTRL_CMD_TEST_MOUSE = 0xA9,
	KBCTRL_CMD_SELF_TEST = 0xAA,
	KBCTRL_CMD_INTERFACE_TEST = 0xAB,
	KBCTRL_CMD_DISABLE_KB = 0xAD,
	KBCTRL_CMD_ENABLE_KB = 0xAE,
	KBCTRL_CMD_READ_INPUT = 0xC0,
	KBCTRL_CMD_READ_OUTPUT = 0xD0,
	KBCTRL_CMD_WRITE_OUTPUT = 0xD1,
	KBCTRL_CMD_WRITE_MOUSE = 0xD4,
	KBCTRL_CMD_SYSTEM_RESET = 0xFE
} kbctrl_t;

typedef enum kbret {
	KBRET_SELF_TEST_OK = 0x55,
	KBRET_ASSURANCE_OK = 0xAA,
	KBRET_ECHO = 0xEE,
	KBRET_ACK = 0xFA,
	KBRET_ASSURANCE_FAILED = 0xFC,
	KBRET_DIAGNOSTIC_FAILED = 0xFD,
	KBRET_RESEND = 0xFE,
	KBRET_KEY_ERROR = 0xFF
} kbret_t;

#define KBCFG_INT					0x01
#define KBCFG_INT2					0x02
#define KBCFG_SYS					0x04
#define KBCFG_UNKNOWN				0x08
#define KBCFG_EN					0x10
#define KBCFG_EN2					0x20
#define KBCFG_XLAT					0x40
#define KBCFG_UNKNOWN2				0x80

#define NOKEY		{ CHTYPE_UNDEFINED, 0 }
#define F(n)		{ CHTYPE_FUNCTION, n}
#define PRINT(c)	{ CHTYPE_PRINT, c }
#define NUMPAD(c)	{ CHTYPE_PRINT_NUMPAD, c }
#define SPEC(k)		{ CHTYPE_SPECIAL, k }
kkb_ch_t scanmap[0x84] = {
	// 00
	NOKEY,
	F(9),
	NOKEY,
	F(5),
	F(3),
	F(1),
	F(2),
	F(12),
	NOKEY,
	F(10),
	F(8),
	F(6),
	F(4),
	{ CHTYPE_CONTROL, '\t' },
	PRINT('`'),
	NOKEY,
	// 10
	NOKEY,
	{ CHTYPE_SHIFT, CHSHIFT_ALT_LEFT },
	{ CHTYPE_SHIFT, CHSHIFT_SHIFT_LEFT },
	NOKEY,
	{ CHTYPE_SHIFT, CHSHIFT_CTRL_LEFT },
	PRINT('q'),
	PRINT('1'),
	NOKEY,
	NOKEY,
	NOKEY,
	PRINT('z'),
	PRINT('s'),
	PRINT('a'),
	PRINT('w'),
	PRINT('2'),
	NOKEY,
	// 20
	NOKEY,
	PRINT('c'),
	PRINT('x'),
	PRINT('d'),
	PRINT('e'),
	PRINT('4'),
	PRINT('3'),
	NOKEY,
	NOKEY,
	PRINT(' '),
	PRINT('v'),
	PRINT('f'),
	PRINT('t'),
	PRINT('r'),
	PRINT('5'),
	NOKEY,
	// 30
	NOKEY,
	PRINT('n'),
	PRINT('b'),
	PRINT('h'),
	PRINT('g'),
	PRINT('y'),
	PRINT('6'),
	NOKEY,
	NOKEY,
	NOKEY,
	PRINT('m'),
	PRINT('j'),
	PRINT('u'),
	PRINT('7'),
	PRINT('8'),
	NOKEY,
	// 40
	NOKEY,
	PRINT(','),
	PRINT('k'),
	PRINT('i'),
	PRINT('o'),
	PRINT('0'),
	PRINT('9'),
	NOKEY,
	NOKEY,
	PRINT('.'),
	PRINT('/'),
	PRINT('l'),
	PRINT(';'),
	PRINT('p'),
	PRINT('-'),
	NOKEY,
	// 50
	NOKEY,
	NOKEY,
	PRINT('\''),
	NOKEY,
	PRINT('['),
	PRINT('='),
	NOKEY,
	NOKEY,
	{ CHTYPE_LOCK, CHLOCK_CAPS },
	{ CHTYPE_SHIFT, CHSHIFT_SHIFT_RIGHT },
	{ CHTYPE_CONTROL, '\n' /* '\r' */ },
	PRINT(']'),
	NOKEY,
	PRINT('#'),
	NOKEY,
	NOKEY,
	// 60
	NOKEY,
	PRINT('\\'),
	NOKEY,
	NOKEY,
	NOKEY,
	NOKEY,
	{ CHTYPE_CONTROL, 8 },
	NOKEY,
	NOKEY,
	NUMPAD('1'),
	NOKEY,
	NUMPAD('4'),
	NUMPAD('7'),
	NOKEY,
	NOKEY,
	NOKEY,
	// 70
	NUMPAD('0'),
	NUMPAD('.'),
	NUMPAD('2'),
	NUMPAD('5'),
	NUMPAD('6'),
	NUMPAD('8'),
	{ CHTYPE_UNPRINTABLE, 27 },
	{ CHTYPE_LOCK, CHLOCK_NUM },
	F(11),
	NUMPAD('+'),
	NUMPAD('3'),
	NUMPAD('-'),
	NUMPAD('*'),
	NUMPAD('9'),
	{ CHTYPE_LOCK, CHLOCK_SCROLL },
	NOKEY,
	// 80
	NOKEY,
	NOKEY,
	NOKEY,
	F(7)
};
kkb_ch_t scanext[0x100] = {
	// 00
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	// 10
	NOKEY, NOKEY, NOKEY, NOKEY,
	{ CHTYPE_SHIFT, CHSHIFT_CTRL_RIGHT }, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, SPEC(CHSPEC_WINKEY),
	// 20
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, SPEC(CHSPEC_WINKEY2),
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, SPEC(CHSPEC_CONTEXT),
	// 30
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	// 40
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NUMPAD('/'), NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	// 50
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, { CHTYPE_CONTROL, '\n' }, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	// 60
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, SPEC(CHSPEC_END), NOKEY, SPEC(CHSPEC_LEFT),
	SPEC(CHSPEC_HOME), NOKEY, NOKEY, NOKEY,
	// 70
	SPEC(CHSPEC_INSERT), SPEC(CHSPEC_DELETE), SPEC(CHSPEC_DOWN), NOKEY,
	SPEC(CHSPEC_RIGHT), SPEC(CHSPEC_UP), NOKEY, NOKEY,
	NOKEY, NOKEY, SPEC(CHSPEC_PAGE_DOWN), NOKEY,
	NOKEY, SPEC(CHSPEC_PAGE_UP), NOKEY, NOKEY,
	// 80
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	// 90
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	// A0
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	// B0
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	// C0
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	// D0
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	// E0
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	// F0
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY,
	NOKEY, NOKEY, NOKEY, NOKEY
};

#define KKB_BUFFERSIZE 100
bool kkb_dual;
bool kkb_extended;
bool kkb_break;
bool caps, shift, alt, ctrl;
kkb_ch_t kkb_buffer[KKB_BUFFERSIZE];
size_t kkb_read, kkb_write;
volatile size_t kkb_available;

u8 i8042_status() {
	return inb(I8042_STATUS);
}

void ctrl_send_cmd(kbctrl_t cmd) {
	while (1) if (!(i8042_status() & KBSTATS_IBF)) break;
	outb(I8042_STATUS, cmd);
}

u8 enc_read_buf() {
	return inb(I8042_DATA);
}

void enc_send_cmd(kbenc_t cmd) {
	while (1) if (!(i8042_status() & KBSTATS_IBF)) break;
	outb(I8042_DATA, cmd);
}

u8 set_leds(bool num, bool caps, bool scroll) {
	u8 data = 0;
	if (scroll)	data |= 1;
	if (num)	data |= 2;
	if (caps)	data |= 4;
	enc_send_cmd(KBENC_CMD_SET_LED);
	enc_send_cmd(data);
	while (1) if (i8042_status() & KBSTATS_OBF) break;
	return enc_read_buf();
}

bool self_test() {
	ctrl_send_cmd(KBCTRL_CMD_SELF_TEST);
	while (1) if (i8042_status() & KBSTATS_OBF) break;
	return enc_read_buf() == KBRET_SELF_TEST_OK;
}

u8 kkb_get_config() {
	ctrl_send_cmd(KBCTRL_CMD_READCMD);
	while (1) if (i8042_status() & KBSTATS_OBF) break;
	return enc_read_buf();
}

void kkb_set_config(u8 cmd) {
	ctrl_send_cmd(KBCTRL_CMD_WRITECMD);
	enc_send_cmd(cmd);
}

void kkb_abort(str message) {
	printf("kkb_abort(%s)", message);
	abort();
}

void kkb_init() {
	// Set up memory stuff
	kkb_dual = false;
	kkb_extended = false;
	kkb_break = false;
	memset((ptr)kkb_buffer, 0, sizeof(kkb_buffer));
	kkb_read = 0;
	kkb_write = 0;
	kkb_available = 0;

	// Initialise USB Controllers (turn off USB Legacy)
	
	// Determine if the PS/2 Controller exists
	
	// Disable devices
	ctrl_send_cmd(KBCTRL_CMD_DISABLE_KB);
	ctrl_send_cmd(KBCTRL_CMD_DISABLE_MOUSE);
	
	// Flush buffer
	enc_read_buf();
	
	// Set Config byte
	u8 cfg = kkb_get_config();
	kkb_dual = cfg & KBCFG_EN2;
	cfg = cfg & ~(KBCFG_INT | KBCFG_INT2 | KBCFG_XLAT);
	kkb_set_config(cfg);
	
	// Perform self-test
	bool success = self_test();
	if (!success) kkb_abort("self_test failed");
	
	// Check dual channel
	if (kkb_dual) {
		ctrl_send_cmd(KBCTRL_CMD_ENABLE_MOUSE);
		cfg = kkb_get_config();
		if (!(cfg & KBCFG_INT2)) {
			kkb_dual = true;
			ctrl_send_cmd(KBCTRL_CMD_DISABLE_MOUSE);
		}
	}
	
	// Perform interface tests
	ctrl_send_cmd(KBCTRL_CMD_INTERFACE_TEST);
	u8 kb_test = enc_read_buf();
	if (kb_test) kkb_abort("kb_test failed");
	if (kkb_dual) {
		ctrl_send_cmd(KBCTRL_CMD_TEST_MOUSE);
		u8 mouse_test = enc_read_buf();
		if (mouse_test) kkb_abort("mouse_test failed");
	}

	// Enable devices
	ctrl_send_cmd(KBCTRL_CMD_ENABLE_KB);
	if (kkb_dual) ctrl_send_cmd(KBCTRL_CMD_ENABLE_MOUSE);
	cfg = kkb_get_config();
	cfg |= KBCFG_INT;
	if (kkb_dual) cfg |= KBCFG_INT2;
	kkb_set_config(cfg);

	// Reset devices
	/* enc_send_cmd(KBENC_CMD_RESET_TEST);
	if (kkb_dual) {
		ctrl_send_cmd(KBCTRL_CMD_WRITE_MOUSE);
		enc_send_cmd(KBENC_CMD_RESET_TEST);
	} */
}

ch kbb_modify(ch src) {
	if (shift) {
		if (!caps)
			switch (src) {
				case '1': return '!';
				case '2': return '"';
				case '3': return '�';
				case '4': return '$';
				case '5': return '%';
				case '6': return '^';
				case '7': return '&';
				case '8': return '*';
				case '9': return '(';
				case '0': return ')';

				case 'q': return 'Q';
				case 'w': return 'W';
				case 'e': return 'E';
				case 'r': return 'R';
				case 't': return 'T';
				case 'y': return 'Y';
				case 'u': return 'U';
				case 'i': return 'I';
				case 'o': return 'O';
				case 'p': return 'P';

				case 'a': return 'A';
				case 's': return 'S';
				case 'd': return 'D';
				case 'f': return 'F';
				case 'g': return 'G';
				case 'h': return 'H';
				case 'j': return 'J';
				case 'k': return 'K';
				case 'l': return 'L';

				case 'z': return 'Z';
				case 'x': return 'X';
				case 'c': return 'C';
				case 'v': return 'V';
				case 'b': return 'B';
				case 'n': return 'N';
				case 'm': return 'M';
			}

		switch (src) {
			case '`': return '�';
			case '-': return '_';
			case '=': return '+';
			case '[': return '{';
			case ']': return '}';
			case ';': return ':';
			case '\'': return '@';
			case '#': return '~';
			case '\\': return '|';
			case ',': return '<';
			case '.': return '>';
			case '/': return '?';
		}
		return src;
	}

	if (caps) {
		switch (src) {
			case 'q': return 'Q';
			case 'w': return 'W';
			case 'e': return 'E';
			case 'r': return 'R';
			case 't': return 'T';
			case 'y': return 'Y';
			case 'u': return 'U';
			case 'i': return 'I';
			case 'o': return 'O';
			case 'p': return 'P';

			case 'a': return 'A';
			case 's': return 'S';
			case 'd': return 'D';
			case 'f': return 'F';
			case 'g': return 'G';
			case 'h': return 'H';
			case 'j': return 'J';
			case 'k': return 'K';
			case 'l': return 'L';

			case 'z': return 'Z';
			case 'x': return 'X';
			case 'c': return 'C';
			case 'v': return 'V';
			case 'b': return 'B';
			case 'n': return 'N';
			case 'm': return 'M';
		}
	}

	if (ctrl && alt) {
		switch (src) {
			case '4': return '�';
			case '`': return '�';
		}
	}

	return src;
}

bool kkb_push(u8 scan) {
	if (scan == 0xE0 || scan == 0xE1) {
		kkb_extended = true;
		return true;
	}
	if (scan == 0xF0) {
		kkb_break = true;
		return true;
	}

	if (kkb_available == KKB_BUFFERSIZE) {
		kkb_extended = false;
		kkb_break = false;
		return false;
	}

	kkb_ch_t key = kkb_extended ? scanext[scan] : scanmap[scan];
	kkb_extended = false;
	if (kkb_break) {
		// interpret shifts
		if (key.type == CHTYPE_SHIFT) {
			if (key.value & CHSHIFT_ALT) alt = false;
			else if (key.value & CHSHIFT_CTRL) ctrl = false;
			else if (key.value & CHSHIFT_SHIFT) shift = false;
		}

		kkb_break = false;
		return true;
	}

	// interpret shifts
	if (key.type == CHTYPE_SHIFT) {
		if (key.value & CHSHIFT_ALT) alt = true;
		else if (key.value & CHSHIFT_CTRL) ctrl = true;
		else if (key.value & CHSHIFT_SHIFT) shift = true;
	}
	if (key.type == CHTYPE_LOCK && key.value == CHLOCK_CAPS) caps = !caps;

	// don't push anything we don't know about
	if (key.type == CHTYPE_UNDEFINED) return false;

	kkb_buffer[kkb_write].type = key.type;
	kkb_buffer[kkb_write].value = key.type == CHTYPE_PRINT ? kbb_modify(key.value) : key.value;
	if (++kkb_write == KKB_BUFFERSIZE) kkb_write = 0;
	kkb_available++;
	return true;
}

u8 kkb_getch() {
kkb_getch_tryagain:
	while (!kkb_available) { }
	kkb_ch_t key = kkb_buffer[kkb_read];
	if (++kkb_read == KKB_BUFFERSIZE) kkb_read = 0;
	kkb_available--;

	if (key.type >= CHTYPE_UNPRINTABLE) goto kkb_getch_tryagain;

	return key.value;
}
