// lagos: kernel/kernel.c
#include <assert.h>
#include <malloc.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <kernel/bda.h>
#include <kernel/gdt.h>
#include <kernel/inlasm.h>
#include <kernel/kkb.h>
#include <kernel/kmm.h>
#include <kernel/multiboot.h>
#include <kernel/rsdp.h>
#include <kernel/tty.h>

#include <driver/pci.h>

multiboot_info_t mb_info;

void kernel_early(u32 magic, multiboot_info_t* info) {
	assert(magic == MULTIBOOT_BOOTLOADER_MAGIC);
	
	/* TODO: is there a better way to do this? kmm_init() tramples
	 * over memory when it writes the memory block preludes */ 
	memcpy(&mb_info, info, sizeof(multiboot_info_t));
	
	terminal_initialize();
	rsdp_init();
	init_descriptor_tables();
	
	kmm_init(info);
	kkb_init();
}

const char* xdigits = "0123456789ABCDEF";

void hex_show(unsigned char c) {
	printf("%c%c ", xdigits[c >> 4], xdigits[c % 16]);
}

void mem_show(unsigned char* ptr, u32 count) {
	size_t ti = 0;
	char text[17];
	memset(text, 0, 17);
	printf("Memory @%x - %x\n", (u32)ptr, (u32)(ptr + count));
	for (u32 i = 0; i < count; i++) {
		unsigned char x = ptr[i];
		hex_show(x);
		text[ti] = x < 32 ? '.' : x;
		if (++ti == 16) {
			puts(text);
			ti = 0;
		}
	}
	if (ti != 0) puts(text);
}

void bda_stats() {
	printf("COM: %x/%x/%x/%x\n", bda->com1, bda->com2, bda->com3, bda->com4);
	printf("LPT: %x/%x/%x\n", bda->lpt1, bda->lpt2, bda->lpt3);
	printf("EBDA: %x\n", bda->ebda);
	printf("VID: %x\n", bda->vidport);
	printf("KEYBUF: %x-%x (@%x until %x)\n\n", bda->keybuf_start, bda->keybuf_end, bda->keybuf_next, bda->keybuf_last);
	printf("MEM: %dkB\n", bda->memsize);
	
	// hardware
	printf("HARDWARE:");
	if (bda->hardware & HW_FLOPPY) printf(" floppy");
	if (bda->hardware & HW_MATH) printf(" mathcopro");
	if (bda->hardware & HW_MOUSE) printf(" ps2mouse");
	if ((bda->hardware & HW_VID) == HW_VID_EGA) printf(" ega");
	if ((bda->hardware & HW_VID) == HW_VID_40_25) printf(" 40x25");
	if ((bda->hardware & HW_VID) == HW_VID_80_25) printf(" 80x25");
	if ((bda->hardware & HW_VID) == HW_VID_MONO) printf(" mono80x25");
	printf("\n");
	
	printf("DISPLAY MODE: %d\n", bda->vidmode);
	printf("TEXT COLS: %d\n", bda->textcols);
	printf("TICKS: %d\n", bda->timer);
	printf("DISKS: %d\n", bda->hd_count);
}

extern void* gdt_addr();

#define SPLIT64(q) (u32)(q >> 32), (u32)(q & 0xFFFFFFFF)

void multiboot_parse() {
	printf("-- MULTIBOOT INFO\n");
	printf("Flags: ");
	if (mb_info.flags & MULTIBOOT_INFO_MEMORY) printf("memory ");
	if (mb_info.flags & MULTIBOOT_INFO_BOOTDEV) printf("bootdev ");
	if (mb_info.flags & MULTIBOOT_INFO_CMDLINE) printf("cmdline ");
	if (mb_info.flags & MULTIBOOT_INFO_MODS) printf("mods ");
	if (mb_info.flags & MULTIBOOT_INFO_AOUT_SYMS) printf("a.out ");
	if (mb_info.flags & MULTIBOOT_INFO_ELF_SHDR) printf("elf ");
	if (mb_info.flags & MULTIBOOT_INFO_MEM_MAP) printf("mmap ");
	if (mb_info.flags & MULTIBOOT_INFO_DRIVE_INFO) printf("drive ");
	if (mb_info.flags & MULTIBOOT_INFO_CONFIG_TABLE) printf("config ");
	if (mb_info.flags & MULTIBOOT_INFO_BOOT_LOADER_NAME) printf("name ");
	if (mb_info.flags & MULTIBOOT_INFO_APM_TABLE) printf("apm ");
	if (mb_info.flags & MULTIBOOT_INFO_VBE_INFO) printf("vbe ");
	if (mb_info.flags & MULTIBOOT_INFO_FRAMEBUFFER_INFO) printf("fb ");
	printf("\n");
	
	if (mb_info.flags & MULTIBOOT_INFO_MEMORY) {
		printf("BIOS Memory: %x/%x\n", mb_info.mem_lower << 10, mb_info.mem_upper << 10);
	}
	
	if (mb_info.flags & MULTIBOOT_INFO_BOOTDEV) {
		printf("Boot Device: %x\n", mb_info.boot_device);
	}
	
	if (mb_info.flags & MULTIBOOT_INFO_CMDLINE) {
		printf("Command Line: %s\n", mb_info.cmdline);
	}
	
	if (mb_info.flags & MULTIBOOT_INFO_MODS) {
		printf("Modules @%x:\n", (u32)mb_info.mods);
		for (u32 i = 0; i < mb_info.mods_count; i++) {
			multiboot_module_t mod = mb_info.mods[i];
			printf("  %x-%x: %s\n", mod.mod_start, mod.mod_end, mod.cmdline);
		}
	}
	
	if (mb_info.flags & MULTIBOOT_INFO_MEM_MAP) {
		u64 total_memory = 0;
		u64 available_memory = 0;
		printf("Memory Map @%x:\n", mb_info.mmap_addr);
		u32 mmap_end = mb_info.mmap_addr + mb_info.mmap_length;
		for (u32 addr = mb_info.mmap_addr; addr < mmap_end; addr += sizeof(multiboot_memory_map_t)) {
			multiboot_memory_map_t* mmap = (multiboot_memory_map_t*)addr;
			printf("  mem@%x%x+%x%x ", SPLIT64(mmap->addr), SPLIT64(mmap->len));
			if (mmap->type == MULTIBOOT_MEMORY_AVAILABLE) printf("available\n");
			else if (mmap->type == MULTIBOOT_MEMORY_RESERVED) printf("reserved\n");
			else if (mmap->type == MULTIBOOT_MEMORY_ACPI_RECLAIMABLE) printf("ACPI reclaimable\n");
			else if (mmap->type == MULTIBOOT_MEMORY_NVS) printf("NVS\n");
			else if (mmap->type == MULTIBOOT_MEMORY_BADRAM) printf("bad RAM\n");
			else printf("????\n");
			
			if (mmap->type == MULTIBOOT_MEMORY_AVAILABLE) available_memory += mmap->len;
			total_memory += mmap->len;
		}
		
		printf("    total memory=%x%x, available=%x%x\n", SPLIT64(total_memory), SPLIT64(available_memory));
	}
	
	if (mb_info.flags & MULTIBOOT_INFO_BOOT_LOADER_NAME) {
		printf("Bootloader: %s\n", mb_info.boot_loader_name);
	}
}

void show_cpuid() {
	u32 a, d;
	char s[17];
	s[16] = 0;
	u32* sa = (u32*)s;
	
	// Vendor ID
	cpuid_string(0x00000000, sa);
	printf("Vendor ID: %s [%x supported]\n", &s[4], *sa);
	
	// CPU features
	cpuid(0x00000001, &a, &d);
	printf("Features:");
	if (d & 0x00000001) printf(" fpu");
	if (d & 0x00000002) printf(" vme");
	if (d & 0x00000004) printf(" de");
	if (d & 0x00000008) printf(" pse");
	if (d & 0x00000010) printf(" tsc");
	if (d & 0x00000020) printf(" msr");
	if (d & 0x00000040) printf(" pae");
	if (d & 0x00000080) printf(" mce");
	if (d & 0x00000100) printf(" cx8");
	if (d & 0x00000200) printf(" apic");
	if (d & 0x00000400) printf(" ?10?");
	if (d & 0x00000800) printf(" sep");
	if (d & 0x00001000) printf(" mtrr");
	if (d & 0x00002000) printf(" pge");
	if (d & 0x00004000) printf(" mca");
	if (d & 0x00008000) printf(" cmov");
	if (d & 0x00010000) printf(" pat");
	if (d & 0x00020000) printf(" pse36");
	if (d & 0x00040000) printf(" psn");
	if (d & 0x00080000) printf(" clf");
	if (d & 0x00100000) printf(" ?20?");
	if (d & 0x00200000) printf(" dtes");
	if (d & 0x00400000) printf(" acpi");
	if (d & 0x00800000) printf(" mmx");
	if (d & 0x01000000) printf(" fxsr");
	if (d & 0x02000000) printf(" sse");
	if (d & 0x04000000) printf(" sse2");
	if (d & 0x08000000) printf(" ss");
	if (d & 0x10000000) printf(" htt");
	if (d & 0x20000000) printf(" tm1");
	if (d & 0x40000000) printf(" ia64");
	if (d & 0x80000000) printf(" pbe");
	printf("\n");
}

ptr kmm_test_a(size_t n) {
	ptr mem = kmm_alloc(n);
	if (mem == 0) printf("  could not allocate %d bytes\n", (int)n);
	else printf("  allocated %d bytes @%x\n", (int)n, (u32)mem);
	return mem;
}

void kmm_test_f(ptr mem, size_t n) {
	if (mem == 0) return;
	kmm_free(mem);
	printf("  freed %d bytes of memory\n", (int)n);
}

void kmm_test() {
	printf("-- kmm_test()\n");
	kmm_info();
	ptr a = kmm_test_a(1024);
	ptr b = kmm_test_a(256);
	kmm_test_f(a, 1024);
	kmm_test_f(b, 256);
	kmm_info();
}

void kernel_main() {
	printf("I love Emily!\n\n");
	
	show_cpuid();
	//multiboot_parse();
	//kb_test();
	
	//kmm_test();
	
	//bda_stats();
	//rsdp_info();

	//printf("-- GDT:\n");
	//gdt_info();
	
	printf("pci_init()\n");
	pci_init();

	printf("Echoing keyboard input.\n");
	while (1) {
		u8 c = kkb_getch();
		terminal_putch(c);
	}
}
