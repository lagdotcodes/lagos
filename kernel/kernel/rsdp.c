// lagos: kernel/rsdp.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <kernel/rsdp.h>
#include <sys/cdefs.h>

rsdp_v1_t* rsdp_v1;
rsdp_v2_t* rsdp_v2;
rsdt_t* rsdt;
#ifdef __64
xsdt_t* xsdt;
#endif

void rsdp_init() {
	int found = 0;
	ptr addr = (ptr)0xE0000;
	while (addr <= (ptr)0xFFFFF) {
		if (!memcmp((void*)addr, RSDP_SIGNATURE, 8)) {
			found = 1;
			break;
		}
		addr += 16;
	}
	if (!found) {
		printf("Could not find RSDP!\n");
		abort();
	}

	rsdp_v1 = (rsdp_v1_t*)addr;
	rsdp_v2 = (rsdp_v2_t*)addr;
	rsdt = (rsdt_t*)(rsdp_v1->rsdt_address);
#ifdef __64
	xsdt = (xsdt_t*)(rsdp_v2->xsdt_address);
#endif
}

void isdt_info();
void rsdp_info() {
	char temp[9];

	printf("--- RSDP\n");
	strncpy(temp, rsdp_v1->signature, 8);
	printf("Signature: %s\n", temp);
	printf("Checksum: %d\n", (int)rsdp_v1->checksum);
	strncpy(temp, rsdp_v1->oem_id, 6);
	printf("OEM ID: %s\n", temp);
	printf("Revision: %d\n", rsdp_v1->revision + 1);
	printf("RSDT@ %x\n", rsdp_v1->rsdt_address);

#ifdef __64
	if (rsdp_v1->revision > 0) {
		printf("--- RSDP v2\n");
		printf("Length: %d\n", rsdp_v2->length);
		printf("XSDT@ %x\n", (int)rsdp_v2->xsdt_address);
		printf("XChecksum: %d\n", (int)rsdp_v2->extended_checksum);

		isdt_info(&xsdt->header);
		int xsdt_entries = (xsdt->header.length - sizeof(isdt_t)) / 8;
		for (int i = 0; i < xsdt_entries; i++) {
			u64 offset = xsdt->tables[i];
			isdt_info((u32)offset);
		}
	} else {
#else
		isdt_info(&rsdt->header);
		int rsdt_entries = (rsdt->header.length - sizeof(isdt_t)) / 4;
		for (int i = 0; i < rsdt_entries; i++) {
			u32 offset = rsdt->tables[i];
			isdt_info(offset);
		}
#endif
#ifdef __64
	}
#endif
}

void isdt_info(isdt_t* t) {
	char sig[5], oem_id[7], oem_table_id[9], creator_id[5];
	
	strncpy(sig, t->signature, 4);
	strncpy(oem_id, t->oem_id, 6);
	strncpy(oem_table_id, t->oem_table_id, 8);
	strncpy(creator_id, t->creator_id, 4);
	printf("@%x | %s r%d | OEM: %s %s r%d | Creator: %s r%d\n", (u32)t, sig, t->revision, oem_id, oem_table_id, t->oem_revision, creator_id, t->creator_revision);
}
