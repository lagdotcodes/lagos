// lagos: kernel/kmm.c
#include <assert.h>
#include <kernel/multiboot.h>
#include <kernel/kmm.h>
#include <stdio.h>

#define BLK_NEW			0x12345678
#define BLK_FREE		0x456789AB
#define BLK_USED		0x789ABCDE

#if __WORDSIZE == 64
#define NULLPTR			0xFFFFFFFFFFFFFFFF
#else
#define NULLPTR			0xFFFFFFFF
#endif
#define NULLBLK			((blk_t*)NULLPTR)

typedef struct kmm_block {
	u32 type;
	size_t length;
	struct kmm_block* next;
} blk_t;
#define blksz			sizeof(blk_t)
#define blk_s(m)		(blk_t*)(m - blksz)
#define blk_start(b)	((ptr)b + blksz)
#define blk_type(b)		(blk->type == BLK_NEW ? "NEW!" : (blk->type == BLK_FREE ? "FREE" : (blk->type == BLK_USED ? "USED" : "????")))

extern u32 _kmm_free;
blk_t* master;
blk_t* last;
size_t total;
size_t in_use;
uptr mstart;

blk_t* kmm_block_init(ptr start, size_t len);
blk_t* kmm_block_find(size_t len);
blk_t* kmm_block_split(blk_t* big, size_t len);
void kmm_block_joins();

#ifdef __64
#define ADDR(n) n
#else
#define ADDR(n) (n & 0xFFFFFFFF)
#endif

void kmm_init(multiboot_info_t* mb) {
	assert(mb->flags & MULTIBOOT_INFO_MEM_MAP);
	
	master = NULLBLK;
	last = NULLBLK;
	total = 0;
	in_use = 0;
	mstart = (uptr)(&_kmm_free);
	
	u32 mmap_end = mb->mmap_addr + mb->mmap_length;
	for (u32 addr = mb->mmap_addr; addr < mmap_end; addr += sizeof(multiboot_memory_map_t)) {
		multiboot_memory_map_t* mmap = (multiboot_memory_map_t*)addr;
		u32 mmaddr = ADDR(mmap->addr);
		u32 mmlen = ADDR(mmap->len);
		if (mmap->type != MULTIBOOT_MEMORY_AVAILABLE) continue;
		if (mmaddr < mstart && (mmaddr + mmlen) > mstart) {
			kmm_block_init((ptr)mstart, mmlen - (mmaddr - mstart));
		} else {
			kmm_block_init((ptr)mmaddr, mmlen);
		}
	}
}

ptr kmm_alloc(size_t n) {
	if (!n) return 0;
	blk_t* blk = kmm_block_find(n);
	if (blk == NULLBLK) {
		//printf("-- kmm_alloc(): could not allocate %d bytes\n", n);
		//abort();
		//__builtin_unreachable();
		return 0;
	}
	
	blk->type = BLK_USED;
	in_use += n;
	return blk_start(blk);
}

void kmm_free(ptr m) {
	if (m == 0) return;
	blk_t* blk = blk_s(m);
	assert(blk->type == BLK_USED);
	blk->type = BLK_FREE;
	in_use -= blk->length;
	
	kmm_block_joins();
}

void kmm_info_block(blk_t* blk) {
	printf("  %s:%x+%x\n", blk_type(blk), (u32)blk_start(blk), (u32)blk->length);
}

void kmm_info() {
	printf("-- kmm_info(): %d/prelude, %d total, %d in use, usable begins @%x\n", (u32)blksz, (u32)total, (u32)in_use, mstart);
	blk_t* blk = master;
	while (blk != NULLBLK) {
		kmm_info_block(blk);
		blk = blk->next;
	}
}

blk_t* kmm_block_init(ptr start, size_t len) {
	blk_t* blk = (blk_t*)start;
	blk->type = BLK_NEW;
	blk->length = len - blksz;
	blk->next = NULLBLK;
	
	total += len;
	in_use += blksz;
	
	if (master == NULLBLK) {
		master = blk;
		last = blk;
	} else {
		last->next = blk;
		last = blk;
	}
	
	return blk;
}

blk_t* kmm_block_find(size_t len) {
	blk_t* blk = master;
	while (blk != NULLBLK) {
		if (blk->type != BLK_USED) {
			if (blk->length == len) return blk;
			if (blk->length > len + blksz) return kmm_block_split(blk, len);
		}
		
		blk = blk->next;
	}
	
	return NULLBLK;
}

blk_t* kmm_block_split(blk_t* big, size_t len) {
	blk_t* small = (blk_t*)(blk_start(big) + len);
	small->type = BLK_NEW;
	small->length = big->length - len - blksz;
	small->next = big->next;
	if (last == big) {
		last = small;
	}
	
	big->length = len;
	big->next = small;
	
	in_use += blksz;
	return big;
}

void kmm_block_joins() {
	blk_t* blk = master;
	while (blk->next != NULLBLK) {
		blk_t* nxt = blk->next;
		if (blk->type != BLK_USED && nxt->type != BLK_USED) {
			if ((ptr)nxt == (ptr)blk + blk->length + blksz) {
				blk->length += blksz + nxt->length;
				in_use -= blksz;
				blk->next = nxt->next;
				continue;
			}
		}
		
		blk = blk->next;
	}
}
