// lagos: kernel/gdt.c
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <kernel/gdt.h>
#include <kernel/inlasm.h>
#include <kernel/kkb.h>

extern void gdt_flush(u32);
extern void idt_flush(u32);

static void init_gdt();
static void gdt_set_gate(u32 num, u32 base, u32 limit, u8 access, u8 gran);

extern void isr0();
extern void isr1();
extern void isr2();
extern void isr3();
extern void isr4();
extern void isr5();
extern void isr6();
extern void isr7();
extern void isr8();
extern void isr9();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr15();
extern void isr16();
extern void isr17();
extern void isr18();
extern void isr19();
extern void isr20();
extern void isr21();
extern void isr22();
extern void isr23();
extern void isr24();
extern void isr25();
extern void isr26();
extern void isr27();
extern void isr28();
extern void isr29();
extern void isr30();
extern void isr31();
extern void isr32();
extern void isr33();
extern void isr34();
extern void isr35();
extern void isr36();
extern void isr37();
extern void isr38();
extern void isr39();
extern void isr40();
extern void isr41();
extern void isr42();
extern void isr43();
extern void isr44();
extern void isr45();
extern void isr46();
extern void isr47();
static void init_idt();
static void idt_set_gate(u8 num, u32 base, u16 sel, u8 flags);

gdt_entry_t	gdt_entries[5];
gdt_ptr_t	gdt_ptr;
idt_entry_t	idt_entries[256];
idt_ptr_t	idt_ptr;

#define ICW1_ICW4		0x01		/* ICW4 (not) needed */
#define ICW1_SINGLE		0x02		/* Single (cascade) mode */
#define ICW1_INTERVAL4	0x04		/* Call address interval 4 (8) */
#define ICW1_LEVEL		0x08		/* Level triggered (edge) mode */
#define ICW1_INIT		0x10		/* Initialization - required! */
 
#define ICW4_8086		0x01		/* 8086/88 (MCS-80/85) mode */
#define ICW4_AUTO		0x02		/* Auto (normal) EOI */
#define ICW4_BUF_SLAVE	0x08		/* Buffered mode/slave */
#define ICW4_BUF_MASTER	0x0C		/* Buffered mode/master */
#define ICW4_SFNM		0x10		/* Special fully nested (not) */

#define PIC1			0x20		/* IO base address for master PIC */
#define PIC2			0xA0		/* IO base address for slave PIC */
#define PIC1_COMMAND	PIC1
#define PIC1_DATA		(PIC1+1)
#define PIC2_COMMAND	PIC2
#define PIC2_DATA		(PIC2+1)
#define PIC_READ_IRR	0x0a		/* OCW3 irq ready next CMD read */
#define PIC_READ_ISR	0x0b		/* OCW3 irq service next CMD read */
#define PIC_EOI			0x20		/* End-of-interrupt command code */

void PIC_sendEOI(u8 irq) {
	if (irq >= 8) outb(PIC2_COMMAND, PIC_EOI);
	outb(PIC1_COMMAND, PIC_EOI);
}

/* Helper func */
static u16 PIC_get_irq_reg(u8 ocw3) {
    /* OCW3 to PIC CMD to get the register values.  PIC2 is chained, and
     * represents IRQs 8-15.  PIC1 is IRQs 0-7, with 2 being the chain */
    outb(PIC1_COMMAND, ocw3);
    outb(PIC2_COMMAND, ocw3);
    return (inb(PIC2_COMMAND) << 8) | inb(PIC1_COMMAND);
}
 
/* Returns the combined value of the cascaded PICs irq request register */
u16 PIC_get_irr() {
    return PIC_get_irq_reg(PIC_READ_IRR);
}
 
/* Returns the combined value of the cascaded PICs in-service register */
u16 PIC_get_isr() {
    return PIC_get_irq_reg(PIC_READ_ISR);
}

void IRQ_set_mask(u8 IRQline) {
    u16 port;
    u8 value;
 
    if(IRQline < 8) {
        port = PIC1_DATA;
    } else {
        port = PIC2_DATA;
        IRQline -= 8;
    }
    value = inb(port) | (1 << IRQline);
    outb(port, value);        
}
 
void IRQ_clear_mask(u8 IRQline) {
    u16 port;
    u8 value;
 
    if(IRQline < 8) {
        port = PIC1_DATA;
    } else {
        port = PIC2_DATA;
        IRQline -= 8;
    }
    value = inb(port) & ~(1 << IRQline);
    outb(port, value);        
}

void PIC_setup(u8 offset1, u8 offset2) {
	outb(PIC1_COMMAND, ICW1_INIT+ICW1_ICW4);	// starts the initialization sequence (in cascade mode)
	io_wait();
	outb(PIC2_COMMAND, ICW1_INIT+ICW1_ICW4);
	io_wait();
	outb(PIC1_DATA, offset1);					// ICW2: Master PIC vector offset
	io_wait();
	outb(PIC2_DATA, offset2);					// ICW2: Slave PIC vector offset
	io_wait();
	outb(PIC1_DATA, 4);							// ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
	io_wait();
	outb(PIC2_DATA, 2);							// ICW3: tell Slave PIC its cascade identity (0000 0010)
	io_wait();
 
	outb(PIC1_DATA, ICW4_8086);
	io_wait();
	outb(PIC2_DATA, ICW4_8086);
	io_wait();

	// turn on all interrupts
	outb(PIC1_DATA, 0);
	io_wait();
	outb(PIC2_DATA, 0);
	io_wait();
}

void init_descriptor_tables() {
	init_gdt();
	init_idt();
	
	PIC_setup(32, 40);
}

static void init_gdt() {
	gdt_ptr.limit = (sizeof(gdt_entry_t) * 5) - 1;
	gdt_ptr.base  = (u32)&gdt_entries;

	gdt_set_gate(0, 0, 0, 0, 0);                // Null segment
	gdt_set_gate(1, 0, 0xFFFFFFFF, 0x9A, 0xCF); // Code segment
	gdt_set_gate(2, 0, 0xFFFFFFFF, 0x92, 0xCF); // Data segment
	gdt_set_gate(3, 0, 0xFFFFFFFF, 0xFA, 0xCF); // User mode code segment
	gdt_set_gate(4, 0, 0xFFFFFFFF, 0xF2, 0xCF); // User mode data segment

	gdt_flush((u32)&gdt_ptr);
}

// Set the value of one GDT entry.
static void gdt_set_gate(u32 num, u32 base, u32 limit, u8 access, u8 gran) {
	gdt_entries[num].base_low		= (base & 0xFFFF);
	gdt_entries[num].base_mid		= (base >> 16) & 0xFF;
	gdt_entries[num].base_high		= (base >> 24) & 0xFF;

	gdt_entries[num].limit_low		= (limit & 0xFFFF);
	gdt_entries[num].granularity	= (limit >> 16) & 0x0F;

	gdt_entries[num].granularity	|= gran & 0xF0;
	gdt_entries[num].access			= access;
}

static void init_idt() {
	idt_ptr.limit = sizeof(idt_entry_t) * 256 -1;
	idt_ptr.base  = (u32)&idt_entries;

	memset(&idt_entries, 0, sizeof(idt_entry_t)*256);

	idt_set_gate( 0, (u32)isr0 , 0x08, 0x8E);
	idt_set_gate( 1, (u32)isr1 , 0x08, 0x8E);
	idt_set_gate( 2, (u32)isr2 , 0x08, 0x8E);
	idt_set_gate( 3, (u32)isr3 , 0x08, 0x8E);
	idt_set_gate( 4, (u32)isr4 , 0x08, 0x8E);
	idt_set_gate( 5, (u32)isr5 , 0x08, 0x8E);
	idt_set_gate( 6, (u32)isr6 , 0x08, 0x8E);
	idt_set_gate( 7, (u32)isr7 , 0x08, 0x8E);
	idt_set_gate( 8, (u32)isr8 , 0x08, 0x8E);
	idt_set_gate( 9, (u32)isr9 , 0x08, 0x8E);
	idt_set_gate(10, (u32)isr10, 0x08, 0x8E);
	idt_set_gate(11, (u32)isr11, 0x08, 0x8E);
	idt_set_gate(12, (u32)isr12, 0x08, 0x8E);
	idt_set_gate(13, (u32)isr13, 0x08, 0x8E);
	idt_set_gate(14, (u32)isr14, 0x08, 0x8E);
	idt_set_gate(15, (u32)isr15, 0x08, 0x8E);
	idt_set_gate(16, (u32)isr16, 0x08, 0x8E);
	idt_set_gate(17, (u32)isr17, 0x08, 0x8E);
	idt_set_gate(18, (u32)isr18, 0x08, 0x8E);
	idt_set_gate(19, (u32)isr19, 0x08, 0x8E);
	idt_set_gate(20, (u32)isr20, 0x08, 0x8E);
	idt_set_gate(21, (u32)isr21, 0x08, 0x8E);
	idt_set_gate(22, (u32)isr22, 0x08, 0x8E);
	idt_set_gate(23, (u32)isr23, 0x08, 0x8E);
	idt_set_gate(24, (u32)isr24, 0x08, 0x8E);
	idt_set_gate(25, (u32)isr25, 0x08, 0x8E);
	idt_set_gate(26, (u32)isr26, 0x08, 0x8E);
	idt_set_gate(27, (u32)isr27, 0x08, 0x8E);
	idt_set_gate(28, (u32)isr28, 0x08, 0x8E);
	idt_set_gate(29, (u32)isr29, 0x08, 0x8E);
	idt_set_gate(30, (u32)isr30, 0x08, 0x8E);
	idt_set_gate(31, (u32)isr31, 0x08, 0x8E);
	idt_set_gate(32, (u32)isr32, 0x08, 0x8E);
	idt_set_gate(33, (u32)isr33, 0x08, 0x8E);
	idt_set_gate(34, (u32)isr34, 0x08, 0x8E);
	idt_set_gate(35, (u32)isr35, 0x08, 0x8E);
	idt_set_gate(36, (u32)isr36, 0x08, 0x8E);
	idt_set_gate(37, (u32)isr37, 0x08, 0x8E);
	idt_set_gate(38, (u32)isr38, 0x08, 0x8E);
	idt_set_gate(39, (u32)isr39, 0x08, 0x8E);
	idt_set_gate(40, (u32)isr40, 0x08, 0x8E);
	idt_set_gate(41, (u32)isr41, 0x08, 0x8E);
	idt_set_gate(42, (u32)isr42, 0x08, 0x8E);
	idt_set_gate(43, (u32)isr43, 0x08, 0x8E);
	idt_set_gate(44, (u32)isr44, 0x08, 0x8E);
	idt_set_gate(45, (u32)isr45, 0x08, 0x8E);
	idt_set_gate(46, (u32)isr46, 0x08, 0x8E);
	idt_set_gate(47, (u32)isr47, 0x08, 0x8E);

	idt_flush((u32)&idt_ptr);
}

static void idt_set_gate(u8 num, u32 base, u16 sel, u8 flags) {
	idt_entries[num].base_lo	= base & 0xFFFF;
	idt_entries[num].base_hi	= (base >> 16) & 0xFFFF;

	idt_entries[num].sel		= sel;
	idt_entries[num].always0	= 0;
	// We must uncomment the OR below when we get to using user-mode.
	// It sets the interrupt gate's privilege level to 3.
	idt_entries[num].flags		= flags /* | 0x60 */;
}

typedef struct registers {
	u32 ds;											// Data segment selector
	u32 edi, esi, ebp, esp, ebx, edx, ecx, eax;		// Pushed by pusha.
	u32 int_no, err_code;							// Interrupt number and error code (if applicable)
	u32 eip, cs, eflags, useresp, ss;				// Pushed by the processor automatically.
} registers_t;

// TODO: figure out why optimizations make this function screw up the stack
void noopt isr_handler(registers_t regs) {
	if (regs.int_no == 0x21) {
		u8 scancode = inb(0x60);
		kkb_push(scancode);
	}
	
	if (regs.int_no >= 0x20) {
		int irq = regs.int_no - 0x20;
		PIC_sendEOI(irq);
	}
} reopt

// OLD STUFF

void gdt_info() {
	gdt_ptr_t* dtr;
#ifdef __GNUC__
	asm("sgdt (%0)" : "=r"(dtr) : : );
#elif defined(_MSC_VER)
	__asm {
		sgdt dtr;
	};
#endif
	printf("GDT/DTR @%x: %d %x\n", (u32)dtr, dtr->limit, dtr->base);
	
	for (int i = 0; i < dtr->limit; i += 8) {
		gdt_entry_t* gdt = (gdt_entry_t*)(dtr->base + i + 1);
		gdt_print(gdt);
	}
}

void gdt_print(gdt_entry_t* gdt) {
	printf("GDT %x+%x ", GDT_BASE(gdt), gdt_len(gdt));
	if (gdt->access & GDTA_CODE) {
		printf("code ");
		printf((gdt->access & GDTA_CONFORMING) ? "conf " : "nonc ");
		printf((gdt->access & GDTA_READABLE) ? "r " : "- ");
	} else {
		printf("data ");
		printf((gdt->access & GDTA_EXPANDDOWN) ? "exdn " : "exup ");
		printf((gdt->access & GDTA_WRITABLE) ? "w " : "- ");
	}
	printf((gdt->access & GDTA_ACCESSED) ? "! " : "- ");
	printf((gdt->access & GDTA_APPLICATION) ? "usr " : "sys ");
	printf((gdt->access & GDTA_PRESENT) ? "mem " : "dsk ");
	int ring = (gdt->access & GDTA_PRIVILEGE) >> 5;
	printf("r%d ", ring);
	printf((gdt->granularity & GDTG_LONG) ? "long " : "shrt ");
	printf((gdt->granularity & GDTG_32BIT) ? "32 " : "16 ");
	printf((gdt->granularity & GDTG_SHIFT) ? "4KiB\n" : "byte\n");
}
