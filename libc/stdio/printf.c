#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

static void print(const char* data, size_t data_length)
{
	for ( size_t i = 0; i < data_length; i++ )
		putchar((int) ((const unsigned char*) data)[i]);
}

#define NUMBER_BUFFER_SZ 33
const char* digits = "0123456789abcdef";
int printf(const char* restrict format, ...)
{
	char number_buffer[NUMBER_BUFFER_SZ];
	va_list parameters;
	va_start(parameters, format);

	int written = 0;
	int divisor = 0, num;
	unsigned int unum;
	size_t amount;
	bool rejected_bad_specifier = false, unsign;

	while ( *format != '\0' )
	{
		if ( *format != '%' )
		{
print_c:
			amount = 1;
			while ( format[amount] && format[amount] != '%' )
				amount++;
			print(format, amount);
			format += amount;
			written += amount;
			continue;
		}

		const char* format_begun_at = format;

		if ( *(++format) == '%' )
			goto print_c;

		if ( rejected_bad_specifier )
		{
incomprehensible_conversion:
			rejected_bad_specifier = true;
			format = format_begun_at;
			goto print_c;
		}

		if ( *format == 'c' )
		{
			format++;
			char c = (char) va_arg(parameters, int /* char promotes to int */);
			print(&c, sizeof(c));
		}
		else if ( *format == 's' )
		{
			format++;
			const char* s = va_arg(parameters, const char*);
			print(s, strlen(s));
		}
		else if (*format == 'd') {
			unsign = false;
			format++;
			divisor = 10;
			goto number;
		}
		else if (*format == 'x') {
			unsign = true;
			format++;
			divisor = 16;
			goto number;
		}
		else if (*format == 'o') {
			unsign = true;
			format++;
			divisor = 8;
			goto number;
		}
		else if (*format == 'b') {
			unsign = true;
			format++;
			divisor = 2;
			goto number;
		} else {
			goto incomprehensible_conversion;
		}
		continue;
		
number:
		num = va_arg(parameters, int);
		if (num < 0 && !unsign) {
			print("-", 1);
			written++;
			num = -num;
		} else if (num == 0) {
			print("0", 1);
			written++;
			continue;
		}
		
		char* number_ptr = number_buffer + NUMBER_BUFFER_SZ;
		int number_count = 0;
		if (unsign) {
			unum = (unsigned int)num;
			while (unum) {
				char dc = digits[unum % divisor];
				number_ptr--;
				*number_ptr = dc;
				number_count++;
				
				unum /= divisor;
			}
		} else {
			while (num) {
				char dc = digits[num % divisor];
				number_ptr--;
				*number_ptr = dc;
				number_count++;
				
				num /= divisor;
			}
		}
		print(number_ptr, number_count);
		written += number_count;
	}

	va_end(parameters);
	return written;
}
