.intel_syntax noprefix  
.section .text
.globl memmove
.type memmove, @function
memmove:
	push edi
	push esi
	mov edi,[esp + 12]		// void* dstptr
	mov esi,[esp + 16]		// void* srcptr
	mov ecx,[esp + 20]		// size_t size
	cmp edi,esi
	jl smaller
	cld		// increase
	jmp loop
smaller:
	add esi,ecx
	add edi,ecx
	std
loop:
	rep movsb
	mov eax,[esp + 12]
	pop esi
	pop edi
	ret
