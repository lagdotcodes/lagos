// lagos/libc: string/strncpy.c
#include <string.h>

char* strncpy(char* dst, const char* src, size_t n) {
	memcpy(dst, src, n);
	dst[n] = 0;
	return dst;
}
