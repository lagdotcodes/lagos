#include <stdio.h>
#include <stdlib.h>
#include <sys/cdefs.h>

noret void abort(void)
{
	// TODO: Add proper kernel panic.
	printf("Kernel Panic: abort()\n");
	while ( 1 ) { }
	__builtin_unreachable();
}
