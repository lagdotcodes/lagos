// lagos: libc/stdlib/malloc.c
#include <assert.h>
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef __is_lagos_kernel
#include <kernel/kmm.h>
#else
#endif

#ifdef __cplusplus
extern "C" {
#endif

void* malloc(size_t size) {
#ifdef __is_lagos_kernel
	return kmm_alloc(size);
#else
	// TODO: userspace malloc()
	printf("malloc(%d)", (int)size);
	abort();
#endif
}

void free(void* mem) {
	if (!mem) return;
#ifdef __is_lagos_kernel
	kmm_free(mem);
#else
	// TODO: userspace free()
	printf("free(%x)", (unsigned int)mem);
	abort();
#endif
}

void* realloc(void* mem, size_t size) {
	// TODO: kmm_realloc(), userspace realloc()
	printf("realloc(%x, %d)", (unsigned int)mem, (int) size);
	abort();
}

void* calloc(size_t nelem, size_t elsize) {
	size_t size = nelem * elsize;		// TODO: overflow
	void* mem = malloc(size);
	memset(mem, 0, size);
	return mem;
}

#ifdef __cplusplus
}
#endif
