// lagos: libc/stdlib/assert.c
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

void __assert(const char* file, int line, const char* expr) {
	printf("[%s:%d] assertion '%s' failed\n", file, line, expr);
	abort();
}
