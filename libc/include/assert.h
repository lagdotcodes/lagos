// lagos: libc/include/assert.h
#ifndef _ASSERT_H
#define _ASSERT_H

#ifdef __cplusplus
extern "C" {
#endif

#if NDEBUG
#define assert(e) ((void) 0)
#else
void __assert(const char* file, int line, const char* expr);
#define assert(e) ((e) ? (void)0 : __assert(__FILE__, __LINE__, #e))
#endif

#ifdef __cplusplus
}
#endif

#endif
