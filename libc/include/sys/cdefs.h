#ifndef _SYS_CDEFS_H
#define _SYS_CDEFS_H 1

#define __lagos_libc 1

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
#if __WORDSIZE == 64
typedef signed long s64;
#else
typedef signed long long s64;
#endif

typedef char ch;
typedef char* str;
typedef void* ptr;

#if __WORDSIZE == 64
#define __64
typedef unsigned long uptr;
#else
#define __32
typedef unsigned int uptr;
#endif

#ifdef __GNUC__
#define struct_packed(name) struct __attribute__((packed)) name
#define noret __attribute__((__noreturn__))
#define noopt __attribute__((optimize("O0")))
#define reopt
#elif defined(_MSC_VER)
#define struct_packed(name) __pragma(pack(push, 1)) struct name __pragma(pack(pop))
#define noret __declspec(noreturn)
#define noopt __pragma(optimize("", off))
#define reopt __pragma(optimize("", on))
#endif

#endif
