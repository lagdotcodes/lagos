#ifndef _MALLOC_H
#define _MALLOC_H 1

#include <stdint.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

void* malloc(size_t size);
void free(void* ptr);
void* realloc(void* ptr, size_t size);
void* calloc(size_t nelem, size_t elsize);

#ifdef __cplusplus
}
#endif

#endif
